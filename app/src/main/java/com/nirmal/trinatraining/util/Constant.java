package com.nirmal.trinatraining.util;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.model.mCart;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.nirmal.trinatraining.activity.HomeActivity.latitute;
import static com.nirmal.trinatraining.activity.HomeActivity.longitute;

public class Constant {

    public static final String PAYPAL_CLIENT_ID = "AUEq00SmkJDJGRycr_VlfiuITnG8m6HwuUu_7is5w2mew1Vxun-DcLAF5SANTQPLFN-AkVAD1754Uyfs";
    public static final String INTENT_FILTER_LOCATION_UPDATE = "intent_filter_location_update";
    public static final String LBM_EVENT_LOCATION_UPDATE = "lbm_event_location_update";
    public static ArrayList<mCart> mCartsList = new ArrayList<>();
    public static List<Address> addresses;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM-DD-YY hh:mm");

    public static String currentTime() {
        String currentDateandTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return currentDateandTime = sdf.format(new Date());
    }

    public static String cOnvert24Time(String stime) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(stime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return displayFormat.format(date);
    }

    public static String aDD15Time(String myTime) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date d = null;
        try {
            d = df.parse(myTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.MINUTE, 15);
        return df.format(cal.getTime());
    }

    public static String currentDate() {
        String currentDateandTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return currentDateandTime = sdf.format(new Date());
    }


    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "MM-dd-YY hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getLatLong2Address(Context context) {
        Geocoder geocoder;
        String sFullAddress = "";
        double latitude = 0.0, longitude = 0.0;
        if (latitute != null) {
            latitude = Double.valueOf(latitute);
            longitude = Double.valueOf(longitute);

            if (context != null) {
                geocoder = new Geocoder(context, Locale.getDefault());
                try {
                    if (latitude != 0.0) {
                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                       /* address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        knownName = addresses.get(0).getFeatureName();*/

                        sFullAddress = addresses.get(0).getAddressLine(0) + "";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sFullAddress;
    }

    public static void InternetDialog(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_internet);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        Button btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public static void CommonDialog(Context context, String sTitle, String sMessage, String btnName) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_common);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvMessage = dialog.findViewById(R.id.tvMessage);
        Button btnClose = dialog.findViewById(R.id.btnClose);

        tvTitle.setText("");
        tvMessage.setText("");
        btnClose.setText("");

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public static File createFolder() {
        String directory = "TrinaTraining";
//        File folderName = new File(Environment.getExternalStorageDirectory(), directory + "/" + FolderName);
        File folderName = new File(Environment.getExternalStorageDirectory(), directory);
        if (!folderName.exists()) {
            folderName.mkdir();
        }
        return folderName;
    }

    public static boolean isInternetON(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo;

        if (manager == null || (networkInfo = manager.getActiveNetworkInfo()) == null) {
            return false;
        }


        if (networkInfo.isConnected()) {
            String typeName = networkInfo.getTypeName();
            if ("WIFI".equalsIgnoreCase(typeName)) {
                return true;
            } else if ("MOBILE".equalsIgnoreCase(typeName)) {
                //String proxyHost = android.net.Proxy.getDefaultHost();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    public static class ProgressLoader {
        ProgressDialog dialog;
        Context context;

        public ProgressLoader(Context context) {
            try {
                this.context = context;
                dialog = new ProgressDialog(context);
            } catch (Exception e) {
                Log.e("ProgressLoader", "Catch");
            }
        }

        public void show() {
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }

        public void Isshow() {
            if (dialog.isShowing() && context != null) {
                dialog.dismiss();
            }
        }

        public void dismiss(Activity context) {
            if (context != null)
                if (!context.isFinishing() && dialog != null && dialog.isShowing())
                    dialog.dismiss();
        }
    }

}
