package com.nirmal.trinatraining.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nirmal.trinatraining.model.mCart;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PreferenceManager {

    public static final String PREFERENCE = "";
    public static final String IS_LOGIN = "islogin";
    public static final String IS_SOCIAL_LOGIN = "is_social_login";
    public static final String IS_INTRO = "isintro";

    public static final String TOKEN = "token";
    public static final String FNAME = "first_name";
    public static final String LNAME = "last_name";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PWS = "pws";
    public static final String PHONE = "phone";
    public static final String UID = "userid";
    public static final String CART_LIST = "cardlist";

    public static final String DEVICE_TYPE = "device_type";
    public static final String DEVICE_ID = "device_id";
    public static final String GOOGLE_ID = "google_id";
    public static final String FACEBOOK_ID = "facebook_id";


    public static final String VIDEO_MEMBERSHIP = "video_membership";
    public static final String REMAING_DAY = "remaing";
    public static final String MEMBERSHIP_ID = "membership_id";
    public static final String PAYMENT_TRANSACTION_ID = "payment_transaction_id";
    public static final String SUB_ID = "payment_sub_id";

    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;

    public PreferenceManager(Context context) {
        sharedPref = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return sharedPref.getString(key, "");
    }

    public void setInt(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {
        return sharedPref.getInt(key, 0);
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public void clearAll() {
        editor.clear().apply();
    }

    public String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void saveArrayList(ArrayList<mCart> list, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();
    }

    public ArrayList<mCart> getArrayList(String key) {
        Gson gson = new Gson();
        String json = sharedPref.getString(key, null);
        Type type = new TypeToken<ArrayList<mCart>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


}
