package com.nirmal.trinatraining.Network;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {

    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("user_register/users/register")
    Call<ResponseBody> callSignup(@Part("username") RequestBody username, @Part("email") RequestBody email,
                                  @Part("password") RequestBody password, @Part("phone") RequestBody phone,
                                  @Part("first_name") RequestBody first_name, @Part("last_name") RequestBody last_name
    );


    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("user_profile_change/edit_profile/user")
    Call<ResponseBody> callEditProfile(
            @Part("user_id") RequestBody user_id,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("user_email") RequestBody user_email,
            @Part("booked_phone") RequestBody booked_phone,
            @Part MultipartBody.Part fileToUpload_1,
            @Part("billing_address_1") RequestBody billing_address_1,
            @Part("change_password") RequestBody change_password,
            @Part("old_password") RequestBody old_password,
            @Part("new_password") RequestBody passwordnew_password);

    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("user_login/user/login")
    Call<ResponseBody> callLogin(@Part("username") RequestBody username,
                                 @Part("password") RequestBody password,
                                 @Part("device_id") RequestBody device_id,
                                 @Part("device_type") RequestBody device_type

    );

    @Multipart
    @POST("user_forget_pass/user/forget_pass")
    Call<ResponseBody> callForgot(@Part("email") RequestBody email);

    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("user_profile/profile/user")
    Call<ResponseBody> callGetProfile(@Part("user_id") RequestBody user_id);

    /*<======================== Home API ================>*/
    // => Home Slider
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("show_slider_image/slider/image")
    Call<ResponseBody> callSlider();

    // => Video:  Check Membership for video
    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("current_user_membership/current_user/membership")
    Call<ResponseBody> callCheckMembership(@Part("user_id") RequestBody user_id);

    /*<========================Video Watching ================>*/

    // => Membership Type (Sliver , Gold)
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("membership_levels/membership/lavel_type")
    Call<ResponseBody> callMembership();

    // => mVideo:  Video List
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("video_service_list/video/service_list")
    Call<ResponseBody> callVideoList();

    /*<======================== Ecommerce ================>*/
    // =>  Product List
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("all_product_details/all_product/details")
    Call<ResponseBody> callEcomProduct();

    /*<======================== Video Editing ================>*/

    // => SubScription:  Video List
    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("new_subcription/subcription/add")
    Call<ResponseBody> callSubScription(
            @Part("user_id") RequestBody user_id,
            @Part("membership_id") RequestBody membership_id,
            @Part("subtotal") RequestBody subtotal,
            @Part("total") RequestBody total,
            @Part("payment_type") RequestBody payment_type,
            @Part("status") RequestBody status,
            @Part("gateway") RequestBody gateway,
            @Part("gateway_environment") RequestBody gateway_environment,
            @Part("payment_transaction_id") RequestBody payment_transaction_id,
            @Part("subscription_transaction_id") RequestBody subscription_transaction_id,
            @Part("startdate") RequestBody startdate,
            @Part("enddate") RequestBody enddate,
            @Part("checkout_id") RequestBody checkout_id);

    /*================Video Editing*/

    // =>Video Editing List

    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("video_list_service/list_video/service")
    Call<ResponseBody> callVideoItem();

    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("cancel_subcription/subcription/cancelled")
    Call<ResponseBody> callCancelMenberShip(
            @Part("user_id") RequestBody user_id,
            @Part("current_membership_id") RequestBody current_membership_id,
            @Part("payment_transaction_id") RequestBody payment_transaction_id,
            @Part("current_subscription_transaction_id") RequestBody current_subscription_transaction_id
    );


    //    @Headers("content-type: application/json;charset=utf-8")
    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("upload_video/upload/video")
    Call<ResponseBody> callVideoUpload(
            @Part("order_id") RequestBody order_id,
            @Part("video_desc") RequestBody video_desc,
            @Part MultipartBody.Part fileToUpload_1);


    /*================ Appoinment Booking ================ */
    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("video_service_order/video_service/order")
    Call<ResponseBody> callOrder(
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("company") RequestBody company,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("address_1") RequestBody address_1,
            @Part("address_2") RequestBody address_2,
            @Part("state") RequestBody state,
            @Part("city") RequestBody city,
            @Part("postcode") RequestBody postcode,
            @Part("country") RequestBody country,
            @Part("customer_id") RequestBody customer_id,
            @Part("product_id") RequestBody product_id,
            @Part("product_qty") RequestBody product_qty,
            @Part("payment_gateway") RequestBody payment_gateway,
            @Part("transaction_key") RequestBody transaction_key
    );

    /*@GET
    Call<SList> isAuthenticated(@Url String url);*/

    @GET
    Call<ResponseBody> isAuthenticated(@Url String url);

    @GET
    Call<ResponseBody> getServiceBookly(@Url String url);

    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("check_order/check/order")
    Call<ResponseBody> callCheckVideoStatus(@Part("user_id") RequestBody user_id);


    @DELETE
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    Call<ResponseBody> deleteAppointment(@Url String url);


    //==============History

    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("user_subscription_order/subscription/user_order")
    Call<ResponseBody> callVideoOrderHistorty(@Part("user_id") RequestBody user_id);


    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("criticalVideoOrderHistory/VideoOrderHistory/order")
    Call<ResponseBody> callCriticalVideoOrderHistorty(@Part("user_id") RequestBody user_id);


    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("ecommenrceHistory_order/ecommenrceHistory/order")
    Call<ResponseBody> callShopVideoOrderHistorty(@Part("user_id") RequestBody user_id);


    //===Edited Video

    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("edited_video/edited/video")
    Call<ResponseBody> callEditedVideoDownload(@Part("order_id") RequestBody order_id);


    //==============Social Login

    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @Multipart
    @POST("login_google/googlelogin/user")
    Call<ResponseBody> callSocialLogin(
            @Part("email") RequestBody email,
            @Part("facebook_id") RequestBody facebook_id,
            @Part("google_id") RequestBody google_id,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("user_name") RequestBody user_name,
            @Part("phone") RequestBody phone,
            @Part("device_type") RequestBody device_type,
            @Part("device_id") RequestBody device_id);

    //===================Appoinment

    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("appoinment/booking/time_slot")
    Call<ResponseBody> callTimeSlot(@Part("booking_date") RequestBody date);


    @Multipart
    @Headers("Authorization: Basic ZGFuaWVsbGVfbWFhc0BtZS5jb206MzIxQE1waXJpY0luZA==")
    @POST("create_appointment/new_appoinment/create")
    Call<ResponseBody> callCreat(
            @Part("staff_id") RequestBody staff_id,
            @Part("staff_any") RequestBody staff_any,
            @Part("service_id") RequestBody service_id,
            @Part("custom_service_name") RequestBody custom_service_name,
            @Part("custom_service_price") RequestBody custom_service_price,
            @Part("start_date") RequestBody start_date,
            @Part("end_date") RequestBody end_date,
            @Part("extras_duration") RequestBody extras_duration,
            @Part("Payment_getway") RequestBody Payment_getway,
            @Part("total_price") RequestBody total_price,
            @Part("paid_price") RequestBody paid_price,
            @Part("status") RequestBody status,
            @Part("customer_id") RequestBody customer_id,
            @Part("number_of_persons") RequestBody number_of_persons,
            @Part("units") RequestBody units,
            @Part("notes") RequestBody notes,
            @Part("customer_name") RequestBody customer_name
    );
}