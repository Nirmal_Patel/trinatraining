package com.nirmal.trinatraining.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.OrderHistorAdapert;
import com.nirmal.trinatraining.model.OrderHistory;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryActivity extends AppCompatActivity {

    RecyclerView rvHistory;
    OrderHistorAdapert oderHistorAdapter;
    ArrayList<OrderHistory> historyList = new ArrayList<>();
    OrderHistory orderHistory;
    ImageView btnBack;

    TextView tvEmptyyCart;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;

    String sStartdate, sEnddate, sName, sStatus,
            sSubscription_transaction_id, sPayment_transaction_id, sInitPayment;

    String sUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        preferenceManager = new PreferenceManager(OrderHistoryActivity.this);
        progressLoader = new Constant.ProgressLoader(OrderHistoryActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        tvEmptyyCart = findViewById(R.id.tvEmptyyCart);
        init();
        callOrderHistorty();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void callOrderHistorty() {

        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);

        Call<ResponseBody> call = apiService.callVideoOrderHistorty(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(OrderHistoryActivity.this);
                try {

                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);


                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);

                            sStartdate = jObj.getString("startdate");
                            sEnddate = jObj.getString("enddate");
                            sName = jObj.getString("name");
                            sStatus = jObj.getString("status");
                            sSubscription_transaction_id = jObj.getString("subscription_transaction_id");
                            sPayment_transaction_id = jObj.getString("payment_transaction_id");
                            sInitPayment = jObj.getString("initial_payment");

                            orderHistory = new OrderHistory(sStartdate, sEnddate, sName, sStatus, sSubscription_transaction_id, sPayment_transaction_id, sInitPayment);
                            historyList.add(orderHistory);
                        }

                        if (!historyList.isEmpty()) {
                            tvEmptyyCart.setVisibility(View.GONE);
                            rvHistory.setVisibility(View.VISIBLE);
                            oderHistorAdapter = new OrderHistorAdapert(OrderHistoryActivity.this, historyList);
                            rvHistory.setAdapter(oderHistorAdapter);
                        } else {
                            tvEmptyyCart.setVisibility(View.VISIBLE);
                            rvHistory.setVisibility(View.GONE);
                        }
                    } else {
                        tvEmptyyCart.setVisibility(View.VISIBLE);
                        rvHistory.setVisibility(View.GONE);
                    }
                    // Toast.makeText(OrderHistoryActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(OrderHistoryActivity.this);
                Toast.makeText(OrderHistoryActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        btnBack = findViewById(R.id.btnBack);
        rvHistory = findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(OrderHistoryActivity.this));
        rvHistory.setNestedScrollingEnabled(false);

       /* rvHistory.addOnItemTouchListener(new RecyclerTouchListener(OrderHistoryActivity.this, rvHistory, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //startActivity(new Intent(OrderHistoryActivity.this, VideoPlayActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));*/

    }
}
