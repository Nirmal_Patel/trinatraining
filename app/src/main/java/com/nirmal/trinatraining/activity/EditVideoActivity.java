package com.nirmal.trinatraining.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.InterFace.ItemListner;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.VideoEditAdapter;
import com.nirmal.trinatraining.model.VideoItem;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.addresses;

public class EditVideoActivity extends AppCompatActivity {

    private static final String TAG = "Edit==>";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    public static VideoItem videoItemTemp = null;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constant.PAYPAL_CLIENT_ID)
            .merchantName("TrinaTraining");
    ImageView btnBack;
    EditText etFName, etLastName, etCompany, etEmail, etContact, etAdd1, etAdd2,
            etState, etCity, etCountry, etPinCode;
    String sUserId;
    RecyclerView rvVideo;
    Constant.ProgressLoader progressLoader;
    PreferenceManager preferenceManager;
    String sId, sPost_title, sPost_content, sPost_date, sImage_url, sReguler_price, sSale_price, gallery_image;
    String tansID = "";
    ArrayList<VideoItem> VideoList = new ArrayList<>();
    VideoItem videoItem;
    VideoEditAdapter videoEditAdapter;
    PayPalPayment thingToBuy;
    String sOrderId = "";
    ImageView btnHistory;
    private String videoId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_video);


        init();

        if (Constant.isInternetON(EditVideoActivity.this)) {
            callVedioEditList();
        } else {
            Constant.InternetDialog(EditVideoActivity.this);
        }

        btnBack.setOnClickListener(view -> onBackPressed());
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditVideoActivity.this, CriticalHistoryActivity.class)
                        .putExtra("from", "critical"));

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void callVedioEditList() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callVideoItem();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(EditVideoActivity.this);
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse:-- " + sResponse);
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("status");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {

                        String sData = obj.getString("data");
                        Log.i(TAG, "onResponse: " + sData);

                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);

                            sId = jObj.getString("ID");
                            sPost_title = jObj.getString("post_title");
                            sPost_content = jObj.getString("post_content");
                            sPost_date = jObj.getString("post_date");
                            sImage_url = jObj.getString("image_url");
                            sReguler_price = jObj.getString("reguler_price");
                            sSale_price = jObj.getString("sale_price");

                            videoItem = new VideoItem(sId, sPost_title, sPost_content,
                                    sPost_date, sImage_url, sReguler_price, sSale_price);
                            VideoList.add(videoItem);


                        }
                        if (!VideoList.isEmpty()) {
                            videoEditAdapter = new VideoEditAdapter(EditVideoActivity.this, VideoList, new ItemListner() {
                                @Override
                                public void onSelected(int pos) {

                                    videoItemTemp = VideoList.get(pos);
                                    videoId = VideoList.get(pos).getId();

                                    proceedPayment(VideoList.get(pos).getReguler_price(), VideoList.get(pos).getPost_title());
                                    //dailogChangeDestination(pos);
                                }
                            });
                            rvVideo.setAdapter(videoEditAdapter);
                        }
                    }
                    //    Toast.makeText(EditVideoActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(EditVideoActivity.this);
                Toast.makeText(EditVideoActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void init() {
        preferenceManager = new PreferenceManager(EditVideoActivity.this);

        sUserId = preferenceManager.getString(PreferenceManager.UID);

        progressLoader = new Constant.ProgressLoader(EditVideoActivity.this);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


        rvVideo = findViewById(R.id.rvVideo);
        btnHistory = findViewById(R.id.btnHistory);
        btnBack = findViewById(R.id.btnBack);
    }

    private void dailogChangeDestination(int pos) {
        final Dialog dialog = new Dialog(EditVideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.raw_payment_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        ImageView imageView = dialog.findViewById(R.id.ivImage);
        ImageView ivClose = dialog.findViewById(R.id.ivClose);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvDescription = dialog.findViewById(R.id.tvDescription);
        TextView tvPaynow = dialog.findViewById(R.id.tvPaynow);
        TextView tvPrice = dialog.findViewById(R.id.tvPrice);

        tvDescription.setText(VideoList.get(pos).getPost_content());
        tvTitle.setText(VideoList.get(pos).getPost_title());
        tvPrice.setText("$" + String.format("%.2f", Double.valueOf(VideoList.get(pos).getReguler_price())));
        Glide.with(EditVideoActivity.this).load(VideoList.get(pos).getImage_url()).into(imageView);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceedPayment(VideoList.get(pos).getReguler_price(), VideoList.get(pos).getPost_title());
            }
        });

        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void proceedPayment(String price, String product) {
        thingToBuy = new PayPalPayment(new BigDecimal(price), "USD", product, PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        JSONObject jsonObject = confirm.toJSONObject();
                        tansID = jsonObject.getJSONObject("response").getString("id");
                        Log.e("tansID", "tansID ===> " + tansID);

                        //showDetailDialog();

                        /*if (Constant.isInternetON(EditVideoActivity.this)) {
                            CallOrder();
                        } else {
                            Constant.InternetDialog(EditVideoActivity.this);
                        }*/

                        startActivity(new Intent(EditVideoActivity.this, ShopPaymnetDoneActivity.class)
                                .putExtra("trans_id", tansID)
                                .putExtra("from", "editvideo")
                        );

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onActivityResult: " + e.getMessage());
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i(TAG, auth.toJSONObject().toString(4));
                        Log.i(TAG, auth.getAuthorizationCode());
                        Toast.makeText(getApplicationContext(), "Future Payment code received from PayPal", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Log.i("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void showDetailDialog() {
        final Dialog dialog = new Dialog(EditVideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.raw_detail_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }

        etFName = dialog.findViewById(R.id.etFirstName);
        etLastName = dialog.findViewById(R.id.etLastName);
        etCompany = dialog.findViewById(R.id.etCompany);
        etEmail = dialog.findViewById(R.id.etEmail);
        etContact = dialog.findViewById(R.id.etContact);
        etAdd1 = dialog.findViewById(R.id.etAdd1);
        etAdd2 = dialog.findViewById(R.id.etAdd2);
        etState = dialog.findViewById(R.id.etState);
        etCity = dialog.findViewById(R.id.etCity);
        etCountry = dialog.findViewById(R.id.etCountry);
        etPinCode = dialog.findViewById(R.id.etPinCode);
        TextView tvNext = dialog.findViewById(R.id.tvNext);

        if (!addresses.isEmpty()) {
            etCity.setText(addresses.get(0).getLocality());
            etState.setText(addresses.get(0).getAdminArea());
            etCountry.setText(addresses.get(0).getCountryName());
            etPinCode.setText(addresses.get(0).getPostalCode());
        } else {
            Constant.getLatLong2Address(EditVideoActivity.this);
        }

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etFName.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "First Name is Required", Toast.LENGTH_LONG).show();
                } else if (etLastName.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Last Name is Required", Toast.LENGTH_LONG).show();
                } else if (etCompany.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Company Name is Required", Toast.LENGTH_LONG).show();
                } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Email is Required", Toast.LENGTH_LONG).show();
                } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Email is Required", Toast.LENGTH_LONG).show();
                } else if (etContact.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Contact No. is Required", Toast.LENGTH_LONG).show();
                } else if (etAdd1.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Address line 1 is Required", Toast.LENGTH_LONG).show();
                } else if (etAdd2.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Address line 2 is Required", Toast.LENGTH_LONG).show();
                } else if (etCity.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "City is Required", Toast.LENGTH_LONG).show();
                } else if (etState.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "State is Required", Toast.LENGTH_LONG).show();
                } else if (etCountry.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Country is Required", Toast.LENGTH_LONG).show();
                } else if (etPinCode.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(EditVideoActivity.this, "Postal Code is Required", Toast.LENGTH_LONG).show();
                } else {
                    if (Constant.isInternetON(EditVideoActivity.this)) {
                        CallOrder();
                    } else {
                        Constant.InternetDialog(EditVideoActivity.this);
                    }
                    dialog.dismiss();
                }
            }
        });
    }

    private void CallOrder() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody companyReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody address_1Req = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody address_2Req = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody stateReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody cityReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody postcodeReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody countryReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody customer_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody product_idReq = RequestBody.create(MediaType.parse("text/plain"), videoId);
        RequestBody product_qtyReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody payment_gatewayReq = RequestBody.create(MediaType.parse("text/plain"), "paypal");
        RequestBody trans_idReq = RequestBody.create(MediaType.parse("text/plain"), tansID);

        Call<ResponseBody> call = apiService.callOrder(
                first_nameReq, last_nameReq, companyReq, emailReq, phoneReq,
                address_1Req, address_2Req, stateReq, cityReq, postcodeReq, countryReq,
                customer_idReq, product_idReq, product_qtyReq, payment_gatewayReq, trans_idReq
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(EditVideoActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");
                    String sVideo_upload = obj.getString("video_upload");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            sOrderId = jsonObject.getString("id");
                        }

                        //if (sVideo_upload.equalsIgnoreCase("Yes")) {
                        startActivity(new Intent(EditVideoActivity.this, VideoUpDownActivity.class).putExtra("oder_id", sOrderId));
                        finish();
                        //}
                    }
                    Toast.makeText(EditVideoActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(EditVideoActivity.this);
                Toast.makeText(EditVideoActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }
}
