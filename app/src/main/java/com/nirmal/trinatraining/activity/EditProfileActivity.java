package com.nirmal.trinatraining.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONObject;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private static final String TAG = "EditProfile==>>";
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1004;
    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 3001;
    EditText etFirstname, etlastname, etEmailId, etPhone, etAddress;
    LinearLayout llUpdate;
    CircleImageView iv_profile;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    String sUsername = "", sUserId;
    TextView tvUsername;
    ImageView btnBack;
    RadioGroup rgPws;
    RadioButton radioButton;
    int selectedId;
    String sSelection, sOldPws = "", sNewPws = "", sFlag = "false";
    LinearLayout llPws;
    EditText etOldPws, etNewPws, etconfirmPws;
    String sUser_login = "", sFirst_name = "", sLast_name = "", sUser_email = "", sBooked_phone = "", sImg = "", sBilling_address_1 = "";
    File file_1 = null;
    RequestBody mRequBody_1 = null;
    MultipartBody.Part fileToUpload_1 = null;
    String path = "";
    LinearLayout llPwsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
        click();

        SetDate();

        selectedId = rgPws.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);

        rgPws.setOnCheckedChangeListener((group, checkedId) -> {
            selectedId = rgPws.getCheckedRadioButtonId();
            radioButton = findViewById(selectedId);
            sSelection = radioButton.getText().toString();
            if (sSelection.equalsIgnoreCase("yes")) {
                llPws.setVisibility(View.VISIBLE);
                sFlag = "true";
            } else {
                llPws.setVisibility(View.GONE);
                sFlag = "false";
            }
        });

        sSelection = radioButton.getText().toString();
        Log.i(TAG, "onCreate: " + sSelection);
    }


    private void SetDate() {

        sUsername = preferenceManager.getString(PreferenceManager.NAME);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        tvUsername.setText(sUsername);
        if (getIntent() != null) {
            etFirstname.setText(getIntent().getStringExtra("fname"));
            etlastname.setText(getIntent().getStringExtra("lname"));
            etEmailId.setText(getIntent().getStringExtra("email"));
            etPhone.setText(getIntent().getStringExtra("phone"));
            etAddress.setText(getIntent().getStringExtra("address"));

            Glide.with(EditProfileActivity.this)
                    .load(getIntent().getStringExtra("img"))
                    .into(iv_profile);
        }
    }

    private void click() {
        llUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    if (Constant.isInternetON(EditProfileActivity.this)) {
                        CallUpdate();
                    } else {
                        Constant.InternetDialog(EditProfileActivity.this);
                    }
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageUpload();
            }
        });
    }

    private void ImageUpload() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            //      intent.setAction(Intent.ACTION_GET_CONTENT);

            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
        }
    }

    private boolean isValid() {


        if (etFirstname.getText().toString().equals("")) {
            etFirstname.setError("Enter First Name .");
            etFirstname.requestFocus();
            return false;
        }

        if (etlastname.getText().toString().equals("")) {
            etlastname.setError("Enter Last Name .");
            etlastname.requestFocus();
            return false;
        }
        if (etEmailId.getText().toString().equals("")) {
            etEmailId.setError("Enter Email Id .");
            etEmailId.requestFocus();
            return false;
        }

        if (etPhone.getText().toString().equals("")) {
            etPhone.setError("Enter Phone No.");
            etPhone.requestFocus();
            return false;
        }
        if (etAddress.getText().toString().equals("")) {
            etAddress.setError("Enter Billing Address.");
            etAddress.requestFocus();
            return false;
        }

        if (sFlag.equals("true")) {
            if (etOldPws.getText().toString().equals("")) {
                etOldPws.setError("Enter Old Password.");
                etOldPws.requestFocus();
                return false;
            }
            if (etNewPws.getText().toString().equals("")) {
                etNewPws.setError("Enter New Password.");
                etNewPws.requestFocus();
                return false;
            }
            if (etconfirmPws.getText().toString().equals("")) {
                etconfirmPws.setError("Enter Confirm Password.");
                etconfirmPws.requestFocus();
                return false;
            }
            if (!etconfirmPws.getText().toString().equals(etNewPws.getText().toString())) {
                etconfirmPws.setError("Confirm Password Not Match.");
                etconfirmPws.requestFocus();
                return false;
            }

        }
        return true;
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.enter, R.anim.exit);
        startActivity(new Intent(EditProfileActivity.this, ProfileActivity.class));
        finish();
        super.onBackPressed();
    }

    private void init() {
        preferenceManager = new PreferenceManager(EditProfileActivity.this);
        progressLoader = new Constant.ProgressLoader(EditProfileActivity.this);
        tvUsername = findViewById(R.id.tvUsername);
        btnBack = findViewById(R.id.btnBack);
        etFirstname = findViewById(R.id.etFirstname);
        etlastname = findViewById(R.id.etlastname);
        etEmailId = findViewById(R.id.etEmailId);
        etPhone = findViewById(R.id.etPhone);
        etAddress = findViewById(R.id.etAddress);
        llUpdate = findViewById(R.id.llUpdate);
        iv_profile = findViewById(R.id.iv_profile);
        rgPws = findViewById(R.id.rgPws);
        llPws = findViewById(R.id.llPws);
        llPwsLayout = findViewById(R.id.llPwsLayout);

        if (preferenceManager.getBoolean(PreferenceManager.IS_SOCIAL_LOGIN)) {
            llPwsLayout.setVisibility(View.GONE);
        } else {
            llPwsLayout.setVisibility(View.VISIBLE);
        }

        etOldPws = findViewById(R.id.etOldPws);
        etNewPws = findViewById(R.id.etNewPws);
        etconfirmPws = findViewById(R.id.etconfirmPws);
    }

    private void CallUpdate() {
        file_1 = new File(path);
        if (file_1.isFile() && file_1.exists()) {
            mRequBody_1 = RequestBody.create(MediaType.parse("multipart/form-data"), file_1);
            fileToUpload_1 = MultipartBody.Part.createFormData("my_image_upload", file_1.getName(), mRequBody_1);
        }

        if (sFlag.equals("true")) {
            sOldPws = etOldPws.getText().toString();
            sNewPws = etNewPws.getText().toString();
        }
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody user_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), etFirstname.getText().toString());
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), etlastname.getText().toString());
        RequestBody user_emailReq = RequestBody.create(MediaType.parse("text/plain"), etEmailId.getText().toString());
        RequestBody booked_phoneReq = RequestBody.create(MediaType.parse("text/plain"), etPhone.getText().toString());

        RequestBody billing_address_1Req = RequestBody.create(MediaType.parse("text/plain"), etAddress.getText().toString());
        RequestBody change_passwordReq = RequestBody.create(MediaType.parse("text/plain"), sFlag);
        RequestBody old_passwordReq = RequestBody.create(MediaType.parse("text/plain"), sOldPws);
        RequestBody new_passwordReq = RequestBody.create(MediaType.parse("text/plain"), sNewPws);

        Call<ResponseBody> call = apiService.callEditProfile(
                user_idReq, first_nameReq, last_nameReq, user_emailReq,
                booked_phoneReq, fileToUpload_1, billing_address_1Req,
                change_passwordReq, old_passwordReq, new_passwordReq);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(EditProfileActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        // callGetDetail();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(EditProfileActivity.this, ProfileActivity.class));
                        finish();
                    }
                    Toast.makeText(EditProfileActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(EditProfileActivity.this);
                Toast.makeText(EditProfileActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();
                String filemanagerstring = selectedImageUri.getPath();
                path = getPath2(selectedImageUri);

                if (filemanagerstring != null) {
                    Glide.with(this).load(selectedImageUri).into(iv_profile);
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    public String getPath2(Uri uri) {
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = this.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

}
