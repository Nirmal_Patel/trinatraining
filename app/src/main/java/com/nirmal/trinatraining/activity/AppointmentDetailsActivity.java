package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.model.getService.GetBooking;

import static com.nirmal.trinatraining.util.Constant.parseDateToddMMyyyy;

public class AppointmentDetailsActivity extends AppCompatActivity {

    GetBooking sList;
    String sFrom = "";
    ImageView btnBack;
    EditText tvTitle, tvDate, tvDuration, tvEndDate, tvPrice, tvStatus, tvNoPersons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);

        Intent intent = getIntent();

        if (intent != null) {
            sList = (GetBooking) intent.getSerializableExtra("appointmentData");
            sFrom = intent.getStringExtra("from");
        }

        init();
        SetData();

        clcik();
    }

    private void clcik() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void SetData() {
        double duration = Double.valueOf(sList.getDuration()) / 60.0;
        tvTitle.setText(sList.getTitle());
        tvPrice.setText("$" + String.format("%.2f", Double.valueOf(sList.getPrice())));
        tvDuration.setText(String.format("%.2f", Double.valueOf(duration + "")) + " min");

        tvDate.setText(parseDateToddMMyyyy(sList.getStart_date()));
        tvEndDate.setText(parseDateToddMMyyyy(sList.getEnd_date()));
        tvNoPersons.setText(sList.getNumber_of_persons() + " Persons");
    }

    private void init() {
        tvTitle = findViewById(R.id.tvTitle);
        tvDate = findViewById(R.id.tvDate);
        tvEndDate = findViewById(R.id.tvEndDate);
        tvDuration = findViewById(R.id.tvDuration);
        tvPrice = findViewById(R.id.tvPrice);
        tvStatus = findViewById(R.id.tvStatus);
        tvNoPersons = findViewById(R.id.tvNoPersons);
        btnBack = findViewById(R.id.btnBack);

        if (sFrom.equals("view")) {
            tvTitle.clearFocus();
            tvTitle.setFocusable(false);
            tvTitle.setEnabled(false);

            tvDate.clearFocus();
            tvDate.setFocusable(false);
            tvDate.setEnabled(false);

            tvDuration.clearFocus();
            tvDuration.setFocusable(false);
            tvDuration.setEnabled(false);

            tvEndDate.clearFocus();
            tvEndDate.setFocusable(false);
            tvEndDate.setEnabled(false);

            tvPrice.clearFocus();
            tvPrice.setFocusable(false);
            tvPrice.setEnabled(false);

            tvStatus.clearFocus();
            tvStatus.setFocusable(false);
            tvStatus.setEnabled(false);

            tvNoPersons.clearFocus();
            tvNoPersons.setFocusable(false);
            tvNoPersons.setEnabled(false);
        } else {

            tvTitle.setFocusable(true);
            tvTitle.setEnabled(true);

            tvDate.setFocusable(true);
            tvDate.setEnabled(true);

            tvDuration.setFocusable(true);
            tvDuration.setEnabled(true);

            tvPrice.setFocusable(true);
            tvPrice.setEnabled(true);

            tvStatus.setFocusable(true);
            tvStatus.setEnabled(true);

            tvNoPersons.setFocusable(true);
            tvNoPersons.setEnabled(true);
        }

    }
}
