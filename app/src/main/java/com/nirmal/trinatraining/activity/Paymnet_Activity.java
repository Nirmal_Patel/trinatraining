package com.nirmal.trinatraining.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.activity.VideoActivity.mMembershipLeveTemp;

public class Paymnet_Activity extends AppCompatActivity {


    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final String TAG = "Paymnet==>>";
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constant.PAYPAL_CLIENT_ID)
            .merchantName("TrinaTraining");
    String sAmount = "1", product = "test", tansID = "";
    PayPalPayment thingToBuy;
    String sUserId;

    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    ImageView iv_playapl;
    Date date1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        setContentView(R.layout.activity_paymnet);

        progressLoader = new Constant.ProgressLoader(Paymnet_Activity.this);
        preferenceManager = new PreferenceManager(Paymnet_Activity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);

        init();


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        ProcessPayment();
    }

    private void ProcessPayment() {
        sAmount = mMembershipLeveTemp.getsInitial_payment();
        product = mMembershipLeveTemp.getsName();

        thingToBuy = new PayPalPayment(new BigDecimal(sAmount), "USD", product,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }


    private void init() {
        double amountDoub = Double.valueOf(mMembershipLeveTemp.getsInitial_payment());
        int amount = (int) amountDoub;
        String month = mMembershipLeveTemp.getsExpiration_number();

        iv_playapl = findViewById(R.id.iv_playapl);

        Glide.with(Paymnet_Activity.this)
                .load(R.drawable.paypal)
                .into(iv_playapl);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {

                    try {
                        JSONObject jsonObject = confirm.toJSONObject();
                        tansID = jsonObject.getJSONObject("response").getString("id");
                        Log.e("tansID", "tansID ===> " + tansID);


                        if (Constant.isInternetON(Paymnet_Activity.this)) {
                            callPayment();
                        } else {
                            Constant.InternetDialog(Paymnet_Activity.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onActivityResult: " + e.getMessage());
                    }

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i(TAG, auth.toJSONObject().toString(4));
                        Log.i(TAG, auth.getAuthorizationCode());
                        Toast.makeText(getApplicationContext(), "Future Payment code received from PayPal", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Log.i("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private int getRandomNumber(int min, int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    @SuppressLint("SimpleDateFormat")
    private void callPayment() {
        progressLoader.show();

        /*try {
            String sCurentDate= Constant.currentTime();
            date1 = new SimpleDateFormat("MM-dd-YY").parse(sCurentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String expDate = getCalculatedDate("yyyy-MM-dd HH:mm:ss",Integer.valueOf(mMembershipLeveTemp.getsExpiration_number()));*/

        //String ss ="2019-11-03 08:15:55";


        String sCurentDate = Constant.currentTime();
        Log.i(TAG, "callPayment: " + mMembershipLeveTemp.getSexpiration_period() + "===" + sCurentDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(sCurentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        int timeLimit = Integer.parseInt(mMembershipLeveTemp.getsExpiration_number());
        if (mMembershipLeveTemp.getSexpiration_period().equals("Month")) {
            calendar.add(Calendar.MONTH, timeLimit);
        } else {
            calendar.add(Calendar.DAY_OF_YEAR, timeLimit);
        }


        Date newDate = calendar.getTime();
        String expDate = dateFormat.format(newDate);

        Log.i(TAG, "onCreate: " + expDate);


        int random = getRandomNumber(100000, 100000000);
        Log.i(TAG, "callPayment: " + random);

        RequestBody user_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody membership_idReq = RequestBody.create(MediaType.parse("text/plain"), mMembershipLeveTemp.getsId());
        RequestBody subtotalReq = RequestBody.create(MediaType.parse("text/plain"), mMembershipLeveTemp.getsInitial_payment());
        RequestBody totalReq = RequestBody.create(MediaType.parse("text/plain"), mMembershipLeveTemp.getsInitial_payment());
        RequestBody payment_typeReq = RequestBody.create(MediaType.parse("text/plain"), "PayPal Standard");
        RequestBody statusReq = RequestBody.create(MediaType.parse("text/plain"), "success");
        RequestBody gatewayReq = RequestBody.create(MediaType.parse("text/plain"), "paypalstandard");
        RequestBody gateway_environmentReq = RequestBody.create(MediaType.parse("text/plain"), "sandbox");
        RequestBody payment_transaction_idReq = RequestBody.create(MediaType.parse("text/plain"), tansID);
        RequestBody subscription_transaction_idReq = RequestBody.create(MediaType.parse("text/plain"), random + "");
        RequestBody startdateReq = RequestBody.create(MediaType.parse("text/plain"), Constant.currentTime());
        RequestBody enddateReq = RequestBody.create(MediaType.parse("text/plain"), expDate + "");
        RequestBody checkout_idReq = RequestBody.create(MediaType.parse("text/plain"), "");

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callSubScription(user_idReq,
                membership_idReq, subtotalReq, totalReq, payment_typeReq,
                statusReq, gatewayReq, gateway_environmentReq,
                payment_transaction_idReq, subscription_transaction_idReq,
                startdateReq, enddateReq, checkout_idReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(Paymnet_Activity.this);
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse:-- " + sResponse);
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        String sStatus = "active";
                        String sRemaing_days = obj.getString("remaing_days");
                        preferenceManager.setString(PreferenceManager.REMAING_DAY, sRemaing_days);
                        preferenceManager.setString(PreferenceManager.VIDEO_MEMBERSHIP, sStatus);

                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(Paymnet_Activity.this, ShopPaymnetDoneActivity.class)
                                .putExtra("trans_id", tansID)
                                .putExtra("from", "subscription"));
                        finish();
                    }
                    //Toast.makeText(Paymnet_Activity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(Paymnet_Activity.this);
                Toast.makeText(Paymnet_Activity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }


    private void ActivatedPlan() {

    }
}
