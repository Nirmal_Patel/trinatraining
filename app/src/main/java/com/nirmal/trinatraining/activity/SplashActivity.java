package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import java.security.MessageDigest;

public class SplashActivity extends AppCompatActivity {

    /*"mpiric321", "mpiric321"*/
    private static final String TAG = "SplashActivity==>>";
    private static final long SPLASH_TIME_OUT = 6000;
    PreferenceManager preferenceManager;
    VideoView video_view;
    Constant.ProgressLoader progressLoader;
    private boolean mIsBackButtonProcessed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_splash);

        progressLoader = new Constant.ProgressLoader(SplashActivity.this);
        preferenceManager = new PreferenceManager(SplashActivity.this);
        init();


        String uriPath = "android.resource://" + getPackageName() + "/" + R.raw.video_b;
        Uri uri = Uri.parse(uriPath);
        video_view.setVideoURI(uri);
        video_view.setMediaController(null);
        video_view.start();


        reDirect();

    }

    private void reDirect() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                if (!mIsBackButtonProcessed) {
                    if (preferenceManager.getBoolean(PreferenceManager.IS_INTRO)) {
                        if (preferenceManager.getBoolean(PreferenceManager.IS_LOGIN)) {
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            video_view.stopPlayback();
                            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            finish();
                        }
                    } else {
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                        finish();
                    }
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void init() {
        video_view = findViewById(R.id.video_view);
        printHashKey();
    }

    public void printHashKey() {
        try {
            final PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                final String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("AppLog", "key:" + hashKey + "=");
            }
        } catch (Exception e) {
            Log.e("AppLog", "error:", e);
        }
    }


    //============Test API =========

}
