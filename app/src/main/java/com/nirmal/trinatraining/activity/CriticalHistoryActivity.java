package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.nirmal.trinatraining.InterFace.ClickListener;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.CriticalHistorAdapert;
import com.nirmal.trinatraining.model.CriticalOrderHistory;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.nirmal.trinatraining.util.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.createFolder;
import static com.nirmal.trinatraining.util.Constant.currentTime;

public class CriticalHistoryActivity extends AppCompatActivity {

    public static String from = "";
    RecyclerView rvHistory;
    CriticalHistorAdapert oderHistorAdapter;
    ArrayList<CriticalOrderHistory> historyList = new ArrayList<>();
    CriticalOrderHistory orderHistory;
    ImageView btnBack;

    TextView tvEmptyyCart;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;

    String sStartdate, sEnddate, sName, sStatus, id, order_key,
            sSubscription_transaction_id, sPayment_transaction_id, sInitPayment, video_service_id = "";

    String sUserId, sOrderId;
    File Folder;
    private int downloadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);


        if (getIntent() != null) {
            from = getIntent().getStringExtra("from");
        } else {
            from = "";
        }

        Folder = createFolder();
        preferenceManager = new PreferenceManager(CriticalHistoryActivity.this);
        progressLoader = new Constant.ProgressLoader(CriticalHistoryActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        tvEmptyyCart = findViewById(R.id.tvEmptyyCart);
        init();

        if (from.equalsIgnoreCase("shop")) {
            callShopOrderHistorty();
        } else {
            callOrderHistorty();
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void callShopOrderHistorty() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);

        Call<ResponseBody> call = apiService.callShopVideoOrderHistorty(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                try {

                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);

                            sStatus = jObj.getString("status");
                            id = jObj.getString("id");
                            order_key = jObj.getString("id");
                            JSONObject jsonObject = new JSONObject(jObj.getString("date_created"));
                            sStartdate = jsonObject.getString("date");

                            sInitPayment = jObj.getString("total");
                            sPayment_transaction_id = jObj.getString("transaction_id");

                            orderHistory = new CriticalOrderHistory(order_key, id, sStartdate, sStatus, sInitPayment, sPayment_transaction_id, "");
                            historyList.add(orderHistory);
                        }

                        if (!historyList.isEmpty()) {
                            tvEmptyyCart.setVisibility(View.GONE);
                            rvHistory.setVisibility(View.VISIBLE);
                            oderHistorAdapter = new CriticalHistorAdapert(CriticalHistoryActivity.this, historyList);
                            rvHistory.setAdapter(oderHistorAdapter);
                        } else {
                            tvEmptyyCart.setVisibility(View.VISIBLE);
                            rvHistory.setVisibility(View.GONE);
                        }
                    } else {
                        tvEmptyyCart.setVisibility(View.VISIBLE);
                        rvHistory.setVisibility(View.GONE);
                    }
                    //Toast.makeText(CriticalHistoryActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                Toast.makeText(CriticalHistoryActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void callOrderHistorty() {

        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);

        Call<ResponseBody> call = apiService.callCriticalVideoOrderHistorty(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                try {

                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);


                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);

                            id = jObj.getString("id");
                            sStatus = jObj.getString("status");
                            order_key = jObj.getString("id");
                            String meta_data = jObj.getString("meta_data");
                            JSONArray jsonArrayMeta = new JSONArray(meta_data);
                            for (int ii = 0; ii < jsonArrayMeta.length(); ii++) {
                                JSONObject jObjMeta = jsonArrayMeta.getJSONObject(ii);
                                String key = jObjMeta.getString("key");
                                if (key.equalsIgnoreCase("video_service_id")) {
                                    video_service_id = jObjMeta.getString("value");
                                }
                            }

                            JSONObject jsonObject = new JSONObject(jObj.getString("date_created"));
                            sStartdate = jsonObject.getString("date");

                            sInitPayment = jObj.getString("total");
                            sPayment_transaction_id = jObj.getString("transaction_id");

                            orderHistory = new CriticalOrderHistory(order_key, id, sStartdate, sStatus,
                                    sInitPayment, sPayment_transaction_id, video_service_id);
                            historyList.add(orderHistory);
                            video_service_id = "";
                        }

                        if (!historyList.isEmpty()) {
                            tvEmptyyCart.setVisibility(View.GONE);
                            rvHistory.setVisibility(View.VISIBLE);
                            oderHistorAdapter = new CriticalHistorAdapert(CriticalHistoryActivity.this, historyList);
                            rvHistory.setAdapter(oderHistorAdapter);
                        } else {
                            tvEmptyyCart.setVisibility(View.VISIBLE);
                            rvHistory.setVisibility(View.GONE);
                        }
                    } else {
                        tvEmptyyCart.setVisibility(View.VISIBLE);
                        rvHistory.setVisibility(View.GONE);
                    }
                    // Toast.makeText(CriticalHistoryActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                Toast.makeText(CriticalHistoryActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void init() {
        btnBack = findViewById(R.id.btnBack);
        rvHistory = findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(CriticalHistoryActivity.this));
        rvHistory.setNestedScrollingEnabled(false);


        if (!from.equalsIgnoreCase("shop")) {
            rvHistory.addOnItemTouchListener(new RecyclerTouchListener(CriticalHistoryActivity.this, rvHistory, new ClickListener() {
                @Override
                public void onClick(View view, final int position) {
                    if (historyList.get(position).getVideo_service_id().equals("")) {
                        sOrderId = historyList.get(position).getId();
                        startActivity(new Intent(CriticalHistoryActivity.this, VideoUpDownActivity.class).putExtra("oder_id", sOrderId));
                        finish();
                    } else {
                        callDownloadVideo(historyList.get(position).getId());
                    }
                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }));

        }
    }

    private void callDownloadVideo(String oderId) {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody orderIdReq = RequestBody.create(MediaType.parse("text/plain"), oderId);

        Call<ResponseBody> call = apiService.callEditedVideoDownload(orderIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                try {
                    if (response.isSuccessful()) {
                        String sResponse = response.body().string();
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("code");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {
                            String sURl = obj.getString("url");
                            Toast.makeText(CriticalHistoryActivity.this, sURl + "", Toast.LENGTH_SHORT).show();

                            if (Folder.exists()) {
                                DownloadFile(sURl);
                            } else {
                                createFolder();
                            }
                        }

                        Toast.makeText(CriticalHistoryActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(CriticalHistoryActivity.this, jObjError.getString("message") + "", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(CriticalHistoryActivity.this);
                Toast.makeText(CriticalHistoryActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void DownloadFile(String sURl) {

        progressLoader.show();
        downloadId = PRDownloader.download(sURl, Folder.getAbsolutePath(), currentTime() + ".mp4")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        progressLoader.Isshow();
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                        //tvShareProgress.setText(progressPercent + " %");
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressLoader.Isshow();
                        Toast.makeText(CriticalHistoryActivity.this, "Video Download Successfully..!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Error error) {
                        progressLoader.Isshow();
                        Toast.makeText(CriticalHistoryActivity.this, "Error while sharing video", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
