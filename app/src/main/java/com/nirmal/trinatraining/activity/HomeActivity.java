package com.nirmal.trinatraining.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.SliderAdapterExample;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.FusedLocationSingleton;
import com.nirmal.trinatraining.util.PermissionManager;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.getLatLong2Address;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "Home==>>";
    public static String latitute, longitute;
    RelativeLayout rvA, rvB, rvC, rvD;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    String sUserId = "", sStartdate, sEnddate, sName, sStatus = "", remaing_days = "";
    SliderAdapterExample adapter;
    ImageView ivProfile;
    private ArrayList<String> stringArrayList = new ArrayList<>();
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Location location = intent.getParcelableExtra(Constant.LBM_EVENT_LOCATION_UPDATE);
                latitute = String.valueOf(location.getLatitude());
                longitute = String.valueOf(location.getLongitude());
                getLatLong2Address(HomeActivity.this);
            } catch (Exception ignored) {
                Log.i(TAG, "onReceive:dffd " + ignored.getMessage());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        preferenceManager = new PreferenceManager(HomeActivity.this);
        progressLoader = new Constant.ProgressLoader(HomeActivity.this);

        sUserId = preferenceManager.getString(PreferenceManager.UID);
        init();
        click();
        FusedLocationSingleton.getInstance().startLocationUpdates();
        LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constant.INTENT_FILTER_LOCATION_UPDATE));

        if (Constant.isInternetON(HomeActivity.this)) {
            CallSlider();
            checkMembership();
            //CallCheckVideoStatus();
        } else {
            Constant.InternetDialog(HomeActivity.this);
        }


    }

    private void CallCheckVideoStatus() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        Call<ResponseBody> call = apiService.callCheckVideoStatus(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.Isshow();
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);
                    String status = obj.getString("status");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            // TODO Working API RESPONCE CHNAGE
                        }
                    }

                } catch (Exception e) {
                    progressLoader.Isshow();
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.Isshow();
                Toast.makeText(HomeActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void CheckPermission() {
        PermissionManager permissionManager = new PermissionManager() {
            @Override
            public void ifCancelledAndCanRequest(Activity activity) {
                super.ifCancelledAndCanRequest(HomeActivity.this);
                Toast.makeText(activity, "Permissions are compulsory, otherwise app might crash", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void ifCancelledAndCannotRequest(Activity activity) {
                super.ifCancelledAndCannotRequest(activity);
                Toast.makeText(activity, "This will cause error in the Application", Toast.LENGTH_SHORT).show();
            }
        };
        permissionManager.checkAndRequestPermissions(HomeActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private void CallSlider() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        Call<ResponseBody> call = apiService.callSlider();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.Isshow();
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);
                    String status = obj.getString("status");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String sIMg = jsonObject.getString("img");
                            stringArrayList.add(sIMg);
                        }
                        slider();
                    }
                } catch (Exception e) {
                    progressLoader.Isshow();
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.Isshow();
                Toast.makeText(HomeActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
        }
        CheckPermission();
    }


    private void checkMembership() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);

        Call<ResponseBody> call = apiService.callCheckMembership(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.Isshow();
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse:-- " + sResponse);
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("status");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            sStartdate = jsonObject.getString("startdate");
                            sEnddate = jsonObject.getString("enddate");
                            sName = jsonObject.getString("name");
                            sStatus = jsonObject.getString("status");
                            remaing_days = jsonObject.getString("remaing_days");
                            preferenceManager.setString(PreferenceManager.REMAING_DAY, remaing_days);
                            if (sStatus.equals("null")) {
                                sStatus = "deactive";
                            }

                            if (sStatus.equals("active")) {
                                String trasn_id = jsonObject.getString("payment_transaction_id");
                                String Subtrasn_id = jsonObject.getString("subscription_transaction_id");
                                String membership_id = jsonObject.getString("membership_id");
                                preferenceManager.setString(PreferenceManager.PAYMENT_TRANSACTION_ID, trasn_id);
                                preferenceManager.setString(PreferenceManager.SUB_ID, Subtrasn_id);
                                preferenceManager.setString(PreferenceManager.MEMBERSHIP_ID, membership_id);
                            }
                        }
                    } else {
                        sStatus = "deactive";
                    }
                    Toast.makeText(HomeActivity.this, "Plan : " + sStatus, Toast.LENGTH_SHORT).show();
                    preferenceManager.setString(PreferenceManager.VIDEO_MEMBERSHIP, sStatus);
                } catch (Exception e) {
                    progressLoader.Isshow();
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.Isshow();
                Toast.makeText(HomeActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void slider() {
        SliderView sliderView = findViewById(R.id.imageSlider);
        adapter = new SliderAdapterExample(this, stringArrayList);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();
    }

    private void click() {
        rvA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, AppoinmentListActivity.class));
            }
        });
        rvB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, EditVideoActivity.class));
            }
        });
        rvC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, EcommerceActivity.class));
            }
        });
        rvD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!sStatus.equals(""))
                    startActivity(new Intent(HomeActivity.this, VideoActivity.class));
            }
        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
        });
    }

    private void init() {
        rvA = findViewById(R.id.rvA);
        rvB = findViewById(R.id.rvB);
        rvC = findViewById(R.id.rvC);
        rvD = findViewById(R.id.rvD);
        ivProfile = findViewById(R.id.ivProfile);
    }


}
