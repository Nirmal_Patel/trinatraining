package com.nirmal.trinatraining.activity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danikula.videocache.HttpProxyCacheServer;
import com.nirmal.trinatraining.App;
import com.nirmal.trinatraining.InterFace.ClickListener;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.MembershipAdapter;
import com.nirmal.trinatraining.adapter.VideoAdapter;
import com.nirmal.trinatraining.model.Vedio;
import com.nirmal.trinatraining.model.mMembershipType;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.nirmal.trinatraining.util.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoActivity extends AppCompatActivity {

    private static final String TAG = "Video==>>";
    public static ArrayList<String> Proxy = new ArrayList<>();
    public static ArrayList<Vedio> videoListTemp = new ArrayList<>();
    public static int pos = 0;
    public static mMembershipType mMembershipLeveTemp = null;
    public String membership = "", isActive = "", sUserId;
    RecyclerView rvVideo, rvMembershipLever;
    VideoAdapter videoAdapter;
    MembershipAdapter membershipAdapter;
    ArrayList<Vedio> videoList = new ArrayList<>();
    HttpProxyCacheServer cacheServer;
    Vedio mVideo;
    TextView tvDays;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    ImageView btnHistory, btnCancel;
    RelativeLayout llMembership, llVideo;
    ArrayList<mMembershipType> mMembershipTypeArrayList = new ArrayList<>();
    mMembershipType mMembershipType;
    String sID, sPost_title, sVideo_url, sPost_content, image_url;
    ImageView btnBack;
    String remaing_days = "", trasn_id = "", Subtrasn_id = "", smembershipId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        setContentView(R.layout.activity_video);

        init();
        cacheServer = App.getCacheServer(this);

        if (!Proxy.isEmpty()) {
            Proxy.clear();
        }

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VideoActivity.this, OrderHistoryActivity.class));
            }
        });

        if (Constant.isInternetON(VideoActivity.this)) {
            callVideoList();
            CheckActiveMembership();
        } else {
            Constant.InternetDialog(VideoActivity.this);
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCancelPlan();
            }
        });
    }

    private void dialogCancelPlan() {
        final Dialog dialog = new Dialog(VideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_cancel_membership);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                CallCancelation();
            }
        });

    }

    private void CallCancelation() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody user_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody current_membership_idReq = RequestBody.create(MediaType.parse("text/plain"), smembershipId);
        RequestBody payment_transaction_idReq = RequestBody.create(MediaType.parse("text/plain"), trasn_id);
        RequestBody current_subscription_transaction_idReq = RequestBody.create(MediaType.parse("text/plain"), Subtrasn_id);


        Log.i(TAG, "CallCancelation: " + smembershipId + "==" +
                trasn_id + "==" +
                Subtrasn_id);
        Call<ResponseBody> call = apiService.callCancelMenberShip(
                user_idReq, current_membership_idReq, payment_transaction_idReq,
                current_subscription_transaction_idReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(VideoActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(VideoActivity.this, HomeActivity.class));
                        finish();
                    }
                    Toast.makeText(VideoActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(VideoActivity.this);
                Toast.makeText(VideoActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void CheckActiveMembership() {
        if (isActive.equals("active")) {
            trasn_id = preferenceManager.getString(PreferenceManager.PAYMENT_TRANSACTION_ID);
            Subtrasn_id = preferenceManager.getString(PreferenceManager.SUB_ID);
            remaing_days = preferenceManager.getString(PreferenceManager.REMAING_DAY);
            smembershipId = preferenceManager.getString(PreferenceManager.MEMBERSHIP_ID);

            tvDays.setText(remaing_days + " Days");
            llVideo.setVisibility(View.VISIBLE);
            llMembership.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        } else {
            btnCancel.setVisibility(View.GONE);
            if (Constant.isInternetON(VideoActivity.this)) {
                callMembership();
            } else {
                Constant.InternetDialog(VideoActivity.this);
            }
        }
    }


    private void callMembership() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callMembership();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(VideoActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);
                    String status = obj.getString("status");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");

                        if (!mMembershipTypeArrayList.isEmpty()) {
                            mMembershipTypeArrayList.clear();
                        }

                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String sId = jsonObject.getString("id");
                            String sName = jsonObject.getString("name");
                            String sInitial_payment = jsonObject.getString("initial_payment");
                            String sExpiration_number = jsonObject.getString("cycle_number");
                            String sexpiration_period = jsonObject.getString("cycle_period");

                            mMembershipType = new mMembershipType(sId, sName, sInitial_payment,
                                    sExpiration_number, sexpiration_period);
                            mMembershipTypeArrayList.add(mMembershipType);
                        }

                        llVideo.setVisibility(View.GONE);
                        llMembership.setVisibility(View.VISIBLE);

                        if (!mMembershipTypeArrayList.isEmpty()) {
                            membershipAdapter = new MembershipAdapter(VideoActivity.this, mMembershipTypeArrayList);
                            rvMembershipLever.setAdapter(membershipAdapter);
                        }
                    }
                    // Toast.makeText(VideoActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(VideoActivity.this);
                Toast.makeText(VideoActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void init() {
        progressLoader = new Constant.ProgressLoader(VideoActivity.this);
        preferenceManager = new PreferenceManager(VideoActivity.this);
        isActive = preferenceManager.getString(PreferenceManager.VIDEO_MEMBERSHIP);
        sUserId = preferenceManager.getString(PreferenceManager.UID);

        llMembership = findViewById(R.id.llMembership);
        btnHistory = findViewById(R.id.btnHistory);
        llVideo = findViewById(R.id.llVideo);
        btnCancel = findViewById(R.id.btnCancel);
        btnBack = findViewById(R.id.btnBack);
        tvDays = findViewById(R.id.tvDayss);

        rvVideo = findViewById(R.id.rvVideo);
        rvVideo.setLayoutManager(new LinearLayoutManager(VideoActivity.this));
        rvVideo.setNestedScrollingEnabled(false);
        rvVideo.addOnItemTouchListener(new RecyclerTouchListener(VideoActivity.this, rvVideo, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                videoListTemp = videoList;
                pos = position;
                startActivity(new Intent(VideoActivity.this, VideoPlayActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        rvMembershipLever = findViewById(R.id.rvMembershipLever);
        rvMembershipLever.setLayoutManager(new LinearLayoutManager(VideoActivity.this));
        rvMembershipLever.setNestedScrollingEnabled(false);
        rvMembershipLever.addOnItemTouchListener(new RecyclerTouchListener(VideoActivity.this, rvMembershipLever, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Paypalpaymnet(mMembershipTypeArrayList.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }


    private void Paypalpaymnet(mMembershipType mMembershipType) {
        mMembershipLeveTemp = mMembershipType;

        DailogConfirmOrder();

    }

    private void DailogConfirmOrder() {
        final Dialog dialog = new Dialog(VideoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_confiem_order);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvDuration = dialog.findViewById(R.id.tvDuration);
        TextView tvExp = dialog.findViewById(R.id.tvExp);

        String month = mMembershipLeveTemp.getsExpiration_number();
        String DayMonth = mMembershipLeveTemp.getSexpiration_period();
        double amountDoub = Double.valueOf(mMembershipLeveTemp.getsInitial_payment());

        tvTitle.setText("Plan : " + mMembershipLeveTemp.getsName());
        tvDuration.setText("Plan : $" + String.format("%.2f", amountDoub) + " / " + month + " " + DayMonth);
        tvExp.setText("Expiration : $ 2 / 1 Day");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(VideoActivity.this, Paymnet_Activity.class));
            }
        });

    }

    private void callVideoList() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callVideoList();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(VideoActivity.this);
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse:-- " + sResponse);
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("status");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {

                        String sData = obj.getString("data");
                        Log.i(TAG, "onResponse: " + sData);

                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            sID = jsonObject.getString("ID");
                            sPost_title = jsonObject.getString("post_title");
                            sVideo_url = jsonObject.getString("video_url");
                            sPost_content = jsonObject.getString("post_content");
                            image_url = jsonObject.getString("image_url");

                            mVideo = new Vedio(sPost_title, "", "", sPost_content, "",
                                    sID, "", sVideo_url, image_url);
                            videoList.add(mVideo);
                        }


                        if (!videoList.isEmpty()) {
                            for (int j = 0; j < videoList.size(); j++) {
                                Proxy.add(cacheServer.getProxyUrl(videoList.get(j).getVideo_url(), true));
                            }
                            videoAdapter = new VideoAdapter(VideoActivity.this, videoList);
                            rvVideo.setAdapter(videoAdapter);
                        }
                    }
                    //Toast.makeText(VideoActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(VideoActivity.this);
                Toast.makeText(VideoActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });


    }

}