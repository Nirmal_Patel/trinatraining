package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nirmal.trinatraining.InterFace.MultiCLickListner;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.Network.ServiceGenerator;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.AppoinmentAdapter;
import com.nirmal.trinatraining.model.getService.GetBooking;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppoinmentListActivity extends AppCompatActivity {

    private static final String TAG = "Appoinment==>>";
    GetBooking getBooking;
    RecyclerView rvAppoinment;
    String sUserId = "";
    AppoinmentAdapter appoinmentAdapter;
    ArrayList<GetBooking> appoinmentList = new ArrayList<>();
    Constant.ProgressLoader progressLoader;
    PreferenceManager preferenceManager;
    FloatingActionButton fabCraeteAppo;
    TextView tvEmpty;
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoinment_list);

        progressLoader = new Constant.ProgressLoader(AppoinmentListActivity.this);
        preferenceManager = new PreferenceManager(AppoinmentListActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);


        if (Constant.isInternetON(AppoinmentListActivity.this)) {
            getAppoinmnet();
        } else {
            Constant.InternetDialog(AppoinmentListActivity.this);
        }

        fabCraeteAppo = findViewById(R.id.fabCraeteAppo);
        tvEmpty = findViewById(R.id.tvEmpty);
        fabCraeteAppo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AppoinmentListActivity.this, AppoinmentActivity.class));
            }
        });
        init();
    }

    private void init() {
        btnBack = findViewById(R.id.btnBack);
        rvAppoinment = findViewById(R.id.rvAppoinment);
        rvAppoinment.setLayoutManager(new LinearLayoutManager(AppoinmentListActivity.this));
        rvAppoinment.setNestedScrollingEnabled(false);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getAppoinmnet() {
        progressLoader.show();
        ApiInterface loginService = ServiceGenerator.createService(ApiInterface.class, "mpiric321", "mpiric321");
        String sURL = "https://trinatraining.com/wp-json/wp/v2/wpo_bookly_appointments?filter[wp_user_id]=" + sUserId;
        Call<ResponseBody> call = loginService.isAuthenticated(sURL);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "onResponse: " + response.body());
                progressLoader.Isshow();
                if (response.isSuccessful()) {

                    try {
                        String sData = response.body().string();
                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);

                            String service_id = jObj.getJSONObject("service_id").getString("id");
                            String category_id = jObj.getJSONObject("service_id").getString("category_id");
                            String title = jObj.getJSONObject("service_id").getString("title");
                            String duration = jObj.getJSONObject("service_id").getString("duration");

                            String price = jObj.getJSONObject("service_id").getString("price");
                            String customer_id = jObj.getJSONObject("customer_appointment").getString("customer_id");
                            String number_of_persons = jObj.getJSONObject("customer_appointment").getString("number_of_persons");
                            String units = jObj.getJSONObject("customer_appointment").getString("units");
                            String status = jObj.getJSONObject("customer_appointment").getString("status");
                            String notes = jObj.getJSONObject("customer_appointment").getString("notes");

                            String start_date = jObj.getString("start_date");
                            String end_date = jObj.getString("end_date");
                            String appoinment_id = jObj.getString("id");

                            getBooking = new GetBooking(service_id, category_id, title, duration,
                                    price, customer_id, number_of_persons, units, status, notes, start_date,
                                    end_date, appoinment_id);
                            appoinmentList.add(getBooking);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (!appoinmentList.isEmpty() && appoinmentList.get(0).getId() != null) {
                        tvEmpty.setVisibility(View.GONE);
                        appoinmentAdapter = new AppoinmentAdapter(AppoinmentListActivity.this, appoinmentList, new MultiCLickListner() {
                            @Override
                            public void onItemClicked(int pos) {
                                Intent i = new Intent(AppoinmentListActivity.this, AppointmentDetailsActivity.class);
                                i.putExtra("appointmentData", appoinmentList.get(pos));
                                i.putExtra("from", "view");
                                startActivity(i);
                            }

                            @Override
                            public void onUpdateClicked(int pos) {
                                Intent i = new Intent(AppoinmentListActivity.this, AppointmentDetailsActivity.class);
                                i.putExtra("appointmentData", appoinmentList.get(pos));
                                i.putExtra("from", "update");
                                startActivity(i);
                            }

                            @Override
                            public void onDeleteCLicked(int pos) {
                                deleteAppointment(pos);
                            }
                        });
                        rvAppoinment.setAdapter(appoinmentAdapter);
                    } else {
                        tvEmpty.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("Error", t.getMessage());
                progressLoader.Isshow();
            }
        });
    }


    private void deleteAppointment(int pos) {
        progressLoader.show();
        String appointmentId = appoinmentList.get(pos).getAppoinment_id();
        ApiInterface deleteService = ServiceGenerator.createService(ApiInterface.class, "mpiric321", "mpiric321");
        String sURL = "https://trinatraining.com/wp-json/wp/v2/wpo_bookly_appointments/" + appointmentId;
        Call<ResponseBody> call = deleteService.deleteAppointment(sURL);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@Nullable Call<ResponseBody> call, @Nullable Response<ResponseBody> response) {
                progressLoader.Isshow();
                Log.i(TAG, "onResponse: " + (response != null ? response.body() : ""));
                if (response != null && response.isSuccessful()) {
                    try {
                        String sResponse = response.body() != null ? response.body().string() : "";
                        JSONObject jsonObject = new JSONObject(sResponse);
                        if (jsonObject.getInt("status") == 200) {
                            Toast.makeText(AppoinmentListActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                            appoinmentList.remove(pos);
                            appoinmentAdapter.notifyDataSetChanged();
                            if (appoinmentList.size() < 1) {
                                tvEmpty.setVisibility(View.VISIBLE);
                            } else {
                                tvEmpty.setVisibility(View.GONE);
                            }
                            Toast.makeText(AppoinmentListActivity.this, "Appoinment Deleted Successfully..!!", Toast.LENGTH_SHORT).show();
                        }
                        progressLoader.Isshow();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressLoader.Isshow();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("DELTTE", "onFailure");
                progressLoader.Isshow();
            }
        });


    }
}
