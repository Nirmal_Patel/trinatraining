package com.nirmal.trinatraining.activity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cbr.gradienttextview.GradientTextView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.hideKeyboard;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 3001;
    private static final String TAG = "Login==>>";
    File file_1 = null;
    RequestBody mRequBody_1 = null;
    MultipartBody.Part fileToUpload_1 = null;
    Dialog dialogg;
    LinearLayout llSignup;
    Button btnLogin, btnGoogle, btnFb;
    GradientTextView tvGradient;
    TextView tvForgot;
    GoogleSignInClient mGoogleSignInClient;
    CallbackManager callbackManager;
    String sDeviceId = "", username = "";
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    Gson gson = new Gson();
    EditText et_name, et_pws;


    String socialId = "", emaildSocial = "", firstName = "",
            lastName = "", gender = "", profilePhoto = "";
    String personName,
            personGivenName,
            personFamilyName,
            personEmail,
            personId,
            personPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (response.getError() != null) {
                                    Log.e("TAG", "facebook response failed error");
                                } else {

                                    Bundle bFacebookData = getFacebookData(me);
                                    Log.e("TAG", "facebook login " + me);
                                    Log.e("TAG", "facebook response " + bFacebookData);

                                    socialId = me.optString("id");
                                    if (bFacebookData != null) {
                                        emaildSocial = bFacebookData.getString("email") != null ? bFacebookData.getString("email") : "";
                                        firstName = bFacebookData.getString("first_name") != null ? bFacebookData.getString("first_name") : "";
                                        lastName = bFacebookData.getString("last_name") != null ? bFacebookData.getString("last_name") : "";
                                        gender = bFacebookData.getString("gender") != null ? bFacebookData.getString("gender") : "";
                                        profilePhoto = bFacebookData.getString("profile_pic") != null ? bFacebookData.getString("profile_pic") : "";
                                    }
                                    if (!socialId.isEmpty()) {
                                        callSocialLogin(profilePhoto, emaildSocial,
                                                socialId, "", firstName,
                                                lastName, "");
                                    }
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        setContentView(R.layout.activity_login);

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        init();
        click();
    }

    private void click() {

        btnGoogle.setOnClickListener(view -> googleSignIn());
        llSignup.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, SignupActivity.class)));
        tvForgot.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ForgotActivity.class)));
        btnFb.setOnClickListener(view -> {
            LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends"));
        });
        btnLogin.setOnClickListener(v -> {
            if (isValid()) {
                hideKeyboard(LoginActivity.this);
                if (Constant.isInternetON(LoginActivity.this)) {
                    callLogin();
                } else {
                    Constant.InternetDialog(LoginActivity.this);
                }
            }
        });
    }


    private void callLogin() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        sDeviceId = preferenceManager.getString(PreferenceManager.DEVICE_ID);

        RequestBody NameReq = RequestBody.create(MediaType.parse("text/plain"), et_name.getText().toString());
        RequestBody PwsReq = RequestBody.create(MediaType.parse("text/plain"), et_pws.getText().toString());
        RequestBody DeviceIdReq = RequestBody.create(MediaType.parse("text/plain"), sDeviceId);
        RequestBody Device_typeReq = RequestBody.create(MediaType.parse("text/plain"), "1");

        Call<ResponseBody> call = apiService.callLogin(NameReq, PwsReq, DeviceIdReq, Device_typeReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(LoginActivity.this);
                try {
                    if (response.isSuccessful()) {
                        String sResponse = response.body().string();
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("code");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {
                            String sUserDetail = obj.getString("user_info");
                            JSONObject jsonObject = new JSONObject(sUserDetail);

                            preferenceManager.setString(PreferenceManager.FNAME, jsonObject.getString("first_name"));
                            preferenceManager.setString(PreferenceManager.LNAME, jsonObject.getString("last_name"));
                            preferenceManager.setString(PreferenceManager.NAME, jsonObject.getString("user_login"));
                            preferenceManager.setString(PreferenceManager.EMAIL, jsonObject.getString("email"));
                            preferenceManager.setString(PreferenceManager.PHONE, jsonObject.getString("phone"));
                            preferenceManager.setString(PreferenceManager.PWS, jsonObject.getString("password"));
                            preferenceManager.setString(PreferenceManager.UID, jsonObject.getString("id"));
                            preferenceManager.setBoolean(PreferenceManager.IS_LOGIN, true);

                            preferenceManager.setBoolean(PreferenceManager.IS_SOCIAL_LOGIN, false);


//                            Toast.makeText(LoginActivity.this, "Login Successfully..!!", Toast.LENGTH_SHORT).show();
                            CommonDialog(LoginActivity.this, "", sMsg + "", "OK");
                        }
                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        CommonDialog(LoginActivity.this, "", jObjError.getString("message") + "",
                                "Cancel");

                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(LoginActivity.this);
                Toast.makeText(LoginActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValid() {
        if (et_name.getText().toString().equals("")) {
            et_name.setError("Enter Username.");
            et_name.requestFocus();
            return false;
        }
        if (et_pws.getText().toString().equals("")) {
            et_pws.setError("Enter Password.");
            et_pws.requestFocus();
            return false;
        }
        return true;
    }

    private void init() {
        preferenceManager = new PreferenceManager(LoginActivity.this);
        progressLoader = new Constant.ProgressLoader(LoginActivity.this);
        llSignup = findViewById(R.id.llSignup);
        btnGoogle = findViewById(R.id.btnGoogle);
        btnFb = findViewById(R.id.btnFb);
        tvGradient = findViewById(R.id.tvGradiant);
        tvForgot = findViewById(R.id.tvForgot);
        btnLogin = findViewById(R.id.btnLogin);

        et_name = findViewById(R.id.et_name);
        et_pws = findViewById(R.id.et_pws);
    }

    public void CommonDialog(Context context, String sTitle, String sMessage, String btnName) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_common);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvMessage = dialog.findViewById(R.id.tvMessage);
        Button btnClose = dialog.findViewById(R.id.btnClose);

        tvTitle.setText("");
        tvMessage.setText(sMessage);
        btnClose.setText(btnName);

        btnClose.setOnClickListener(view -> {
            if (btnName.equalsIgnoreCase("OK")) {
                dialog.dismiss();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            } else {
                dialog.dismiss();
            }
        });
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                personName = account.getDisplayName() != null ? account.getDisplayName() : "";
                personGivenName = account.getGivenName() != null ? account.getGivenName() : "";
                personFamilyName = account.getFamilyName() != null ? account.getFamilyName() : "";
                personEmail = account.getEmail() != null ? account.getEmail() : "";
                personId = account.getId() != null ? account.getId() : "";
                personPhoto = account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : "";

                callSocialLogin(personPhoto, personEmail,
                        "", personId, personGivenName,
                        personFamilyName, "");
            }

        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getLocalizedMessage());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("error", connectionResult.toString());
    }

    private void callSocialLogin(String userImage, String email,
                                 String faceBookid, String googleID,
                                 String firstname, String lastName,
                                 String phoneNo) {
        progressLoader.show();

        long millis = System.currentTimeMillis();
        long seconds = millis / 1000;

        if (!googleID.equals("")) {
            username = firstname + "_gog_" + seconds;
        } else {
            username = firstname + "_fb_" + seconds;
        }
        sDeviceId = preferenceManager.getString(PreferenceManager.DEVICE_ID);


        Log.i(TAG, "callSocialLogin: " +
                email + "\n" + faceBookid + "\n" + googleID + "\n" + firstname + "\n" +
                lastName + "\n" + username + "\n" + phoneNo + "\n" + sDeviceId);


        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody facebook_idReq = RequestBody.create(MediaType.parse("text/plain"), faceBookid);
        RequestBody google_idReq = RequestBody.create(MediaType.parse("text/plain"), googleID);
        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), firstname);
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), lastName);
        RequestBody user_nameReq = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phoneNo);
        RequestBody device_typeReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody device_idReq = RequestBody.create(MediaType.parse("text/plain"), sDeviceId);

        Call<ResponseBody> call = apiService.callSocialLogin(
                emailReq, facebook_idReq, google_idReq, first_nameReq, last_nameReq,
                user_nameReq, phoneReq, device_typeReq, device_idReq
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(LoginActivity.this);
                Log.i(TAG, "onResponse: " + response);


                try {
                    if (response.isSuccessful()) {
                        String sResponse = response.body().string();
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("code");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {
                            String sUserDetail = obj.getString("data");
                            JSONObject jsonObject = new JSONObject(sUserDetail);

                            String sid = jsonObject.getString("id");
                            String sphone = jsonObject.getString("phone");
                            String sdevice_id = jsonObject.getString("device_id");
                            String sgoogle_id = jsonObject.getString("google_id");
                            String sfacebook_id = jsonObject.getString("facebook_id");
                            String sfirst_name = jsonObject.getString("first_name");
                            String slast_name = jsonObject.getString("last_name");
                            String semail = jsonObject.getString("email");
                            String suser_login = jsonObject.getString("user_login");

                            preferenceManager.setString(PreferenceManager.FNAME, jsonObject.getString("first_name"));
                            preferenceManager.setString(PreferenceManager.LNAME, jsonObject.getString("last_name"));
                            preferenceManager.setString(PreferenceManager.NAME, jsonObject.getString("user_login"));
                            preferenceManager.setString(PreferenceManager.EMAIL, jsonObject.getString("email"));
                            preferenceManager.setString(PreferenceManager.PHONE, jsonObject.getString("phone"));
                            preferenceManager.setString(PreferenceManager.UID, jsonObject.getString("id"));
                            preferenceManager.setBoolean(PreferenceManager.IS_LOGIN, true);
                            preferenceManager.setBoolean(PreferenceManager.IS_SOCIAL_LOGIN, true);

                            if (sfirst_name.equalsIgnoreCase("") ||
                                    slast_name.equalsIgnoreCase("") ||
                                    semail.equalsIgnoreCase("") ||
                                    suser_login.equalsIgnoreCase("") ||
                                    sphone.equalsIgnoreCase("")) {
                                updateInfoDialog(false, sgoogle_id, sfacebook_id, sfirst_name,
                                        slast_name, suser_login, semail, sphone);
                            }
                        }

                        Toast.makeText(LoginActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();

                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(LoginActivity.this, jObjError.getString("message") + "", Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(LoginActivity.this);
                Toast.makeText(LoginActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {
        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");
            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=300&height=250");
                Log.e("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                Log.e("MalformedURLException", e + "");
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));
            return bundle;
        } catch (JSONException e) {
            Log.e("exception", e.toString() + " == ");
            return null;
        }
    }

    private void updateInfoDialog(Boolean isFb, String sGoogleId, String sFacebookId,
                                  String firstName, String lastName,
                                  String username, String email, String PhoneNumber) {

        dialogg = new Dialog(LoginActivity.this);
        dialogg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogg.setContentView(R.layout.social_info);
        dialogg.setCancelable(false);
        dialogg.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialogg.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        EditText et_fname = dialogg.findViewById(R.id.et_fname);
        EditText et_lname = dialogg.findViewById(R.id.et_lname);
        EditText et_name = dialogg.findViewById(R.id.et_name);
        EditText et_email = dialogg.findViewById(R.id.et_email);
        EditText et_phone = dialogg.findViewById(R.id.et_phone);
        EditText etAddress = dialogg.findViewById(R.id.etAddress);
        Button btnSubmit = dialogg.findViewById(R.id.btnSubmit);

        et_name.clearFocus();
        et_name.setFocusable(false);
        et_name.setEnabled(false);

        et_fname.setText(firstName);
        et_lname.setText(lastName);
        et_name.setText(username);
        et_phone.setText(PhoneNumber);
        et_email.setText(email);

        btnSubmit.setOnClickListener(view -> {
            if (et_fname.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(LoginActivity.this, "First name required", Toast.LENGTH_SHORT).show();
            } else if (et_lname.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(LoginActivity.this, "Last name required", Toast.LENGTH_SHORT).show();
            } else if (et_email.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(LoginActivity.this, "Email required", Toast.LENGTH_SHORT).show();
            } else if (et_phone.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(LoginActivity.this, "Phone No required", Toast.LENGTH_SHORT).show();
            } else {
                String sUserId = preferenceManager.getString(PreferenceManager.UID);
                CallUpdate(sUserId, et_fname.getText().toString(),
                        et_lname.getText().toString(), et_email.getText().toString(),
                        et_phone.getText().toString(), etAddress.getText().toString());
            }
        });
        dialogg.show();

        Window window = dialogg.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void CallUpdate(String sUserId, String sFirstname,
                            String sLastname, String sEmailId,
                            String sPhone, String sAddress) {
        /*file_1 = new File(path);
        if (file_1.isFile() && file_1.exists()) {
            mRequBody_1 = RequestBody.create(MediaType.parse("multipart/form-data"), file_1);
            fileToUpload_1 = MultipartBody.Part.createFormData("my_image_upload", file_1.getName(), mRequBody_1);
        }*/

        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody user_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), sFirstname);
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), sLastname);
        RequestBody user_emailReq = RequestBody.create(MediaType.parse("text/plain"), sEmailId);
        RequestBody booked_phoneReq = RequestBody.create(MediaType.parse("text/plain"), sPhone);

        RequestBody billing_address_1Req = RequestBody.create(MediaType.parse("text/plain"), sAddress);
        RequestBody change_passwordReq = RequestBody.create(MediaType.parse("text/plain"), "false");
        RequestBody old_passwordReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody new_passwordReq = RequestBody.create(MediaType.parse("text/plain"), "");

        Call<ResponseBody> call = apiService.callEditProfile(
                user_idReq, first_nameReq, last_nameReq, user_emailReq,
                booked_phoneReq, null, billing_address_1Req,
                change_passwordReq, old_passwordReq, new_passwordReq);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(LoginActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (dialogg != null) {
                        dialogg.dismiss();
                    }

                    if (status.equals("200")) {
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        finish();
                    }
                    Toast.makeText(LoginActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(LoginActivity.this);
                Toast.makeText(LoginActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }
}
