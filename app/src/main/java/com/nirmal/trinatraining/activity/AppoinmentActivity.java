package com.nirmal.trinatraining.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.ItemListner;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.Network.ServiceGenerator;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.ServiceAdapter;
import com.nirmal.trinatraining.adapter.SlotAdapter;
import com.nirmal.trinatraining.model.ServiceListItem;
import com.nirmal.trinatraining.model.SlotListItem;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.shuhart.stepview.StepView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.CommonDialog;
import static com.nirmal.trinatraining.util.Constant.aDD15Time;
import static com.nirmal.trinatraining.util.Constant.cOnvert24Time;

public class AppoinmentActivity extends AppCompatActivity {

    private static final String TAG = "Appoi==>>";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constant.PAYPAL_CLIENT_ID)
            .merchantName("TrinaTraining");
    List<ServiceListItem> serviceList = new ArrayList();
    String sUserId = "", sUserName = "";
    StepView step_view;
    Button btnBack, btnNext, btn1, btn2, btn3, btn4, btn5;
    Boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true, bool5 = true;
    int Count = 0;
    String sAmount = "10";
    Constant.ProgressLoader progressLoader;
    SlotAdapter adapter;
    Spinner spStartTime, spFinishTime;
    String sTimeSlotSelected = "", sService = "", sStartTime = "", sFinishTime = "", DAY1 = "", DAY2 = "", DAY3 = "", DAY4 = "", DAY5 = "", product = "test";
    EditText etStartDate, etSlotDate;
    EditText etName, etPhone, etEmail, etNote;
    RecyclerView rlSlots, rvService;
    Calendar calendar = Calendar.getInstance();
    LinearLayout llService, llSlot, llDetail, llpayment, llFinal;
    TextView tvPayName, tvPayPlanName, tvPayDate, tvPayTime, tvPayContact, tvPayEmail, tvService, tvDate, tvTime;
    ArrayList<SlotListItem> sTimeSlot = new ArrayList<>();
    ServiceListItem selectedService;
    ServiceAdapter serviceAdapter;
    Dialog paymentDialog;
    TextView tvNoteSlot, tvTimetext;
    PreferenceManager preferenceManager;
    PayPalPayment thingToBuy;

    ImageView ivBack;
    Button btnS1, btnS2, btnS3;

    RelativeLayout rvTime, rvSlotTime;
    LinearLayout llPrice;
    EditText etPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appoinment);

        preferenceManager = new PreferenceManager(AppoinmentActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        sUserName = preferenceManager.getString(PreferenceManager.NAME);

        init();

        stepProgess();

        sStartTime = spStartTime.getSelectedItem().toString();
        sFinishTime = spFinishTime.getSelectedItem().toString();

        click();
    }


    private void init() {
        progressLoader = new Constant.ProgressLoader(AppoinmentActivity.this);

        btnS1 = findViewById(R.id.btnS1);
        btnS2 = findViewById(R.id.btnS2);
        btnS3 = findViewById(R.id.btnS3);
        llPrice = findViewById(R.id.llPrice);
        etPrice = findViewById(R.id.etPrice);


        if (Constant.isInternetON(AppoinmentActivity.this)) {
            callGetServiceAppimnet();
        } else {
            Constant.InternetDialog(AppoinmentActivity.this);
        }


        step_view = findViewById(R.id.step_view);
        btnBack = findViewById(R.id.btnBack);
        ivBack = findViewById(R.id.ivBack);
        btnNext = findViewById(R.id.btnNext);

        rvTime = findViewById(R.id.rvTime);
        rvSlotTime = findViewById(R.id.rvSlotTime);
        rvService = findViewById(R.id.rvService);


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


        etStartDate = findViewById(R.id.etStartDate);
        etName = findViewById(R.id.etName);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);
        etNote = findViewById(R.id.etNote);

        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);


        tvPayName = findViewById(R.id.tvPayName);
        tvPayPlanName = findViewById(R.id.tvPayPlanName);
        tvPayDate = findViewById(R.id.tvPayDate);
        tvPayTime = findViewById(R.id.tvPayTime);
        tvPayContact = findViewById(R.id.tvPayContact);
        tvPayEmail = findViewById(R.id.tvPayEmail);
        spStartTime = findViewById(R.id.spStartTime);
        spFinishTime = findViewById(R.id.spFinishTime);
        tvService = findViewById(R.id.tvService);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);

        etSlotDate = findViewById(R.id.etSlotDate);
        rlSlots = findViewById(R.id.rlSlots);
        tvNoteSlot = findViewById(R.id.tvNoteSlot);
        tvTimetext = findViewById(R.id.tvTimetext);

        llService = findViewById(R.id.llService);
        llService.setVisibility(View.VISIBLE);
        llSlot = findViewById(R.id.llSlot);
        llDetail = findViewById(R.id.llDetail);
        llpayment = findViewById(R.id.llpayment);
        llFinal = findViewById(R.id.llFinal);

        etStartDate.setText(Constant.currentDate());
        etSlotDate.setText(etStartDate.getText().toString());

    }

    private void DateChoose(final EditText editTextt) {
        int mYear = calendar.get(Calendar.YEAR);
        final int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AppoinmentActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String sTime = (monthOfYear + 1) + "-" + dayOfMonth + "-" + year;

                editTextt.setText(sTime);
                etStartDate.setText(sTime);
                etSlotDate.setText(sTime);

                callTimeSlot(editTextt.getText().toString());
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void click() {
        btnS1.setOnClickListener(view -> ChageColor("1"));
        btnS2.setOnClickListener(view -> ChageColor("2"));
        btnS3.setOnClickListener(view -> ChageColor("3"));


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        rvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.hideKeyboard(AppoinmentActivity.this);
                DateChoose(etStartDate);
            }
        });

        rvSlotTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.hideKeyboard(AppoinmentActivity.this);
                DateChoose(etSlotDate);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Count > 0) {
                    Count--;
                }

                Log.e("count_Back", "count --->" + Count);
                step_view.go(Count, true);

                if (Count == 0) {
                    btnBack.setVisibility(View.GONE);
                    step_view.done(false);
                } else {
                    btnBack.setVisibility(View.VISIBLE);
                }

                if (Count == 0) {
                    btnNext.setVisibility(View.VISIBLE);
                    llService.setVisibility(View.VISIBLE);
                    llSlot.setVisibility(View.GONE);
                    llDetail.setVisibility(View.GONE);
                    llpayment.setVisibility(View.GONE);
                    llFinal.setVisibility(View.GONE);

                } else if (Count == 1) {
                    llService.setVisibility(View.GONE);
                    llSlot.setVisibility(View.VISIBLE);
                    llDetail.setVisibility(View.GONE);
                    llpayment.setVisibility(View.GONE);
                    llFinal.setVisibility(View.GONE);
                } else if (Count == 2) {
                    llService.setVisibility(View.GONE);
                    llSlot.setVisibility(View.GONE);
                    llpayment.setVisibility(View.VISIBLE);
                    llDetail.setVisibility(View.GONE);
                    llFinal.setVisibility(View.GONE);

                } else if (Count == 3) {
                    llService.setVisibility(View.GONE);
                    llSlot.setVisibility(View.GONE);
                    llDetail.setVisibility(View.GONE);
                    llpayment.setVisibility(View.GONE);
                    llFinal.setVisibility(View.VISIBLE);

                }

            }
        });
        btnNext.setOnClickListener(view -> {
            if (Count < 4) {
                Count++;
            }
            Log.e("count", "count ===>" + Count + "==" + step_view.getState());

            step_view.go(Count, true);
            if (Count == 3) {
                step_view.done(true);
                btnNext.setText("Finish");
            } else {
                if (Count == 0) {
                    btnBack.setVisibility(View.GONE);
                } else {
                    btnBack.setVisibility(View.VISIBLE);
                }
            }

            if (Count == 0) {
                llService.setVisibility(View.VISIBLE);
                llDetail.setVisibility(View.GONE);
                llpayment.setVisibility(View.GONE);
                llFinal.setVisibility(View.GONE);

            } else if (Count == 1) {
                if (selectedService == null) {
                    Toast.makeText(this, "Please Select Service.", Toast.LENGTH_SHORT).show();
                    btnBack.setVisibility(View.GONE);
                    Count = 0;
                    step_view.go(Count, true);
                } else {
                    btnBack.setVisibility(View.VISIBLE);
                    callTimeSlot(etStartDate.getText().toString());
                    llService.setVisibility(View.GONE);
                    llSlot.setVisibility(View.VISIBLE);
                    llDetail.setVisibility(View.GONE);
                    llpayment.setVisibility(View.GONE);
                    llFinal.setVisibility(View.GONE);
                }
            } else if (Count == 2) {
                if (sTimeSlotSelected.equals("")) {
                    Toast.makeText(this, "Please Select time Slot.", Toast.LENGTH_SHORT).show();
                    Count = 1;
                    step_view.go(Count, true);
                } else {
                    llService.setVisibility(View.GONE);
                    llSlot.setVisibility(View.GONE);
                    llDetail.setVisibility(View.VISIBLE);
                    llpayment.setVisibility(View.GONE);
                    llFinal.setVisibility(View.GONE);
                }
            } else if (Count == 3) {
                llService.setVisibility(View.GONE);
                llSlot.setVisibility(View.GONE);
                llDetail.setVisibility(View.GONE);
                btnBack.setVisibility(View.GONE);
                DialogConfirmData();
            } else if (Count == 4) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                Intent myintent = new Intent(AppoinmentActivity.this, AppoinmentListActivity.class);
                myintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(myintent);
                finish();
            }

            Log.i(TAG, "clickDataNext: " + sService + "\n==" + etSlotDate.getText().toString() + "\n=="
                    + sTimeSlotSelected + "\n==" + etName.getText().toString() + "\n==" +
                    etPhone.getText().toString() + "\n==" + etEmail.getText().toString() + "\n==" +
                    etNote.getText().toString());
        });


    }

    private void DialogConfirmData() {
        paymentDialog = new Dialog(AppoinmentActivity.this);
        paymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paymentDialog.setContentView(R.layout.dailog_confirm_detail);
        paymentDialog.setCancelable(false);
        paymentDialog.setCanceledOnTouchOutside(false);
        Window window = paymentDialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        Objects.requireNonNull(paymentDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        paymentDialog.show();


        Button btnPayment = paymentDialog.findViewById(R.id.btnPayment);
        Button btnCancel = paymentDialog.findViewById(R.id.btnCancel);
        TextView tvService = paymentDialog.findViewById(R.id.tvService);
        TextView tvDate = paymentDialog.findViewById(R.id.tvDate);
        TextView tvTime = paymentDialog.findViewById(R.id.tvTime);
        TextView tvName = paymentDialog.findViewById(R.id.tvName);
        TextView tvPhone = paymentDialog.findViewById(R.id.tvPhone);
        TextView tvEmail = paymentDialog.findViewById(R.id.tvEmail);
        TextView tvPrice = paymentDialog.findViewById(R.id.tvPrice);

        tvService.setText(selectedService.getTitle());
        tvPrice.setText("$" + String.format("%.2f", Double.valueOf(selectedService.getPrice())));
        tvDate.setText(etSlotDate.getText().toString());
        tvTime.setText(sTimeSlotSelected);
        tvName.setText(etName.getText().toString());
        tvPhone.setText(etPhone.getText().toString());
        tvEmail.setText(etEmail.getText().toString());
        btnPayment.setOnClickListener(view -> ProcessPayment());

        btnCancel.setOnClickListener(view -> {
            overridePendingTransition(R.anim.enter, R.anim.exit);
            Intent myintent = new Intent(AppoinmentActivity.this, HomeActivity.class);
            myintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(myintent);
            finish();
        });
    }

    private void ChageColor(String sType) {
        btnS1.setBackgroundResource(R.drawable.btn_border);
        btnS1.setTextColor(Color.parseColor("#8F66EC"));

        btnS2.setBackgroundResource(R.drawable.btn_border);
        btnS2.setTextColor(Color.parseColor("#8F66EC"));

        btnS3.setBackgroundResource(R.drawable.btn_border);
        btnS3.setTextColor(Color.parseColor("#8F66EC"));


        if (sType.equals("2")) {
            btnS2.setBackgroundResource(R.drawable.btn_border_select);
            btnS2.setTextColor(Color.parseColor("#ffffff"));
            sService = btnS2.getText().toString();
        } else if (sType.equals("3")) {
            btnS3.setBackgroundResource(R.drawable.btn_border_select);
            btnS3.setTextColor(Color.parseColor("#ffffff"));
            sService = btnS3.getText().toString();
        } else {
            btnS1.setBackgroundResource(R.drawable.btn_border_select);
            btnS1.setTextColor(Color.parseColor("#ffffff"));
            sService = btnS1.getText().toString();
        }
    }

    private void stepProgess() {
        step_view.getState()
                .selectedTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                .selectedCircleColor(ContextCompat.getColor(this, R.color.colorAccent))
                .selectedCircleRadius(getResources().getDimensionPixelSize(R.dimen.dp14))
                .selectedStepNumberColor(ContextCompat.getColor(this, R.color.white))
                .steps(new ArrayList<String>() {{
                    add("Service");
                    add("Time");
                    add("Detail");
                    add("Done");
                }})
                .stepsNumber(4)
                .animationType(StepView.ANIMATION_CIRCLE)
                .animationDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .stepLineWidth(getResources().getDimensionPixelSize(R.dimen.dp1))
                .textSize(getResources().getDimensionPixelSize(R.dimen.sp14))
                .stepNumberTextSize(getResources().getDimensionPixelSize(R.dimen.sp16))
                .commit();
    }

    private void ProcessPayment() {


        thingToBuy = new PayPalPayment(new BigDecimal(sAmount), "USD",
                product, PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    paymentDialog.dismiss();

                    Log.e("id", confirm.getProofOfPayment().getPaymentId());
                    callCreateAppimnet();

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                llService.setVisibility(View.VISIBLE);
                llpayment.setVisibility(View.GONE);
                llSlot.setVisibility(View.GONE);
                llFinal.setVisibility(View.GONE);
                llDetail.setVisibility(View.GONE);
                btnNext.setText("Next");
                Count = 0;
                step_view.go(Count, true);


                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                llpayment.setVisibility(View.GONE);
                llFinal.setVisibility(View.VISIBLE);
                llService.setVisibility(View.GONE);
                llDetail.setVisibility(View.GONE);
                btnNext.setText("Next");

                Count--;
                step_view.go(Count, true);

            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i(TAG, auth.toJSONObject().toString(4));
                        Log.i(TAG, auth.getAuthorizationCode());
                        Toast.makeText(getApplicationContext(), "Future Payment code received from PayPal", Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        Log.i("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }


    private void callTimeSlot(String sDate) {
        progressLoader.show();
        rlSlots.setVisibility(View.GONE);
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody SlotDateReq = RequestBody.create(MediaType.parse("text/plain"), sDate);

        Call<ResponseBody> call = apiService.callTimeSlot(SlotDateReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.Isshow();
                try {
                    if (response.isSuccessful()) {
                        String sResponse = response.body().string();
                        Log.i(TAG, "Slot : onResponse: " + sResponse);
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("code");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {
                            rlSlots.setVisibility(View.VISIBLE);
                            if (!sTimeSlot.isEmpty()) {
                                sTimeSlot.clear();
                            }

                            String sSlotObj = obj.getString("available_booking_time");
                            int is_holiday = obj.getInt("is_holiday");

                            if (is_holiday == 0) {
                                tvNoteSlot.setVisibility(View.GONE);
                                tvTimetext.setVisibility(View.VISIBLE);
                                rlSlots.setVisibility(View.VISIBLE);
                                btnNext.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = new JSONArray(sSlotObj);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String sSlot = jsonObject.getString("slot");
                                    sTimeSlot.add(new SlotListItem(sSlot, false));
                                }
                                adapter = new SlotAdapter(sTimeSlot, AppoinmentActivity.this, new ItemListner() {
                                    @Override
                                    public void onSelected(int pos) {
                                        for (int i = 0; i < sTimeSlot.size(); i++) {
                                            {

                                                if (i == pos) {
                                                    sTimeSlot.get(i).setSelected(true);
                                                    sTimeSlotSelected = sTimeSlot.get(i).getTime();
                                                } else {
                                                    sTimeSlot.get(i).setSelected(false);
                                                }
                                            }
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                                rlSlots.setAdapter(adapter);


                            } else {
                                tvTimetext.setVisibility(View.GONE);
                                rlSlots.setVisibility(View.GONE);
                                btnNext.setVisibility(View.GONE);
                                tvNoteSlot.setVisibility(View.VISIBLE);
                            }


                        }
                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(AppoinmentActivity.this, jObjError.getString("message") + "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                    progressLoader.Isshow();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AppoinmentActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
                progressLoader.Isshow();
            }
        });
    }


    private void callCreateAppimnet() {
        progressLoader.show();

        String Start24Time = cOnvert24Time(sTimeSlotSelected);
        String End24Time = cOnvert24Time(sTimeSlotSelected);
        String sStartDate = etSlotDate.getText().toString() + " " + Start24Time;
        String sEndDate = etSlotDate.getText().toString() + " " + aDD15Time(End24Time);

        Log.i(TAG, "callCreateAppimnet: " + selectedService.getPrice() + "\n" +
                "\n" + sStartDate + "\n" + sEndDate);

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody staff_idReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody staff_anyReq = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody service_idReq = RequestBody.create(MediaType.parse("text/plain"), selectedService.getId());
        RequestBody custom_service_nameReq = RequestBody.create(MediaType.parse("text/plain"), selectedService.getTitle());
        RequestBody custom_service_priceReq = RequestBody.create(MediaType.parse("text/plain"), selectedService.getPrice());
        RequestBody start_dateReq = RequestBody.create(MediaType.parse("text/plain"), sStartDate);
        RequestBody end_dateReq = RequestBody.create(MediaType.parse("text/plain"), sEndDate);
        RequestBody extras_durationReq = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody Payment_getwayReq = RequestBody.create(MediaType.parse("text/plain"), "paypal");
        RequestBody total_priceReq = RequestBody.create(MediaType.parse("text/plain"), selectedService.getPrice());
        RequestBody paid_priceReq = RequestBody.create(MediaType.parse("text/plain"), selectedService.getPrice());
        RequestBody statusReq = RequestBody.create(MediaType.parse("text/plain"), "approved");
        RequestBody customer_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody number_of_personsReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody unitsReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody notesReq = RequestBody.create(MediaType.parse("text/plain"), etNote.getText().toString());
        RequestBody customer_nameReq = RequestBody.create(MediaType.parse("text/plain"), sUserName);

        Call<ResponseBody> call = apiService.callCreat(
                staff_idReq, staff_anyReq, service_idReq, custom_service_nameReq,
                custom_service_priceReq, start_dateReq, end_dateReq, extras_durationReq,
                Payment_getwayReq, total_priceReq, paid_priceReq, statusReq, customer_idReq,
                number_of_personsReq, unitsReq, notesReq, customer_nameReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(AppoinmentActivity.this);
                try {
                    if (response.isSuccessful()) {
                        String sResponse = response.body().string();
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("status");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {

                            Log.i(TAG, "onResponse: " + Count);
                            step_view.go(3, true);
                            llpayment.setVisibility(View.GONE);
                            llFinal.setVisibility(View.VISIBLE);
                            tvService.setText(selectedService.getTitle());
                            tvDate.setText(etStartDate.getText().toString());
                            tvTime.setText(sTimeSlotSelected);
                            btnBack.setVisibility(View.GONE);
                        }
                        Toast.makeText(AppoinmentActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        CommonDialog(AppoinmentActivity.this, "", jObjError.getString("message") + "",
                                "Cancel");

                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(AppoinmentActivity.this);
                Toast.makeText(AppoinmentActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void callGetServiceAppimnet() {
        progressLoader.show();
        ApiInterface loginService = ServiceGenerator.createService(ApiInterface.class,
                "mpiric321", "mpiric321");

        String sURL = "https://trinatraining.com/wp-json/wp/v2/wpo_bookly_services/";
        Call<ResponseBody> call = loginService.getServiceBookly(sURL);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "onResponse: " + response.body());
                progressLoader.Isshow();
                if (response.isSuccessful()) {

                    try {
                        String sResponse = response.body().string();
                        Log.i(TAG, "onResponse: " + sResponse);
                        JSONArray jsonArray = new JSONArray(sResponse);
                        if (!serviceList.isEmpty()) {
                            serviceList.clear();
                        }

                        for (int i = 0; i < jsonArray.length(); i++) {
                            String id = "";
                            String price = "";
                            String title = "";
                            String duration = "";

                            id = jsonArray.getJSONObject(i).getString("id");
                            title = jsonArray.getJSONObject(i).getString("title");
                            price = jsonArray.getJSONObject(i).getString("price");
                            duration = jsonArray.getJSONObject(i).getString("duration");
                            serviceList.add(new ServiceListItem(id, price, title, duration, false));


                        }


                        serviceAdapter = new ServiceAdapter(AppoinmentActivity.this, serviceList, new ItemListner() {
                            @Override
                            public void onSelected(int pos) {
                                for (int i = 0; i < serviceList.size(); i++) {
                                    if (i == pos) {
                                        serviceList.get(i).setSelected(true);
                                        llPrice.setVisibility(View.VISIBLE);
                                        etPrice.setText("$" + String.format("%.2f", Double.valueOf(serviceList.get(i).getPrice())));
                                    } else {
                                        serviceList.get(i).setSelected(false);
                                    }
                                }
                                serviceAdapter.notifyDataSetChanged();
                                selectedService = serviceList.get(pos);
                            }
                        });


                        rvService.setLayoutManager(new LinearLayoutManager(AppoinmentActivity.this));
                        rvService.setAdapter(serviceAdapter);


                        Log.e("jsonSize", "jsonSize -->  " + jsonArray.length());


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("Error", t.getMessage());
                progressLoader.Isshow();
            }
        });


    }


}
