package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    ImageView btnBack, btnEdit;
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    String sUserId = "", sUsername = "";

    LinearLayout llLogout;

    String sUser_login = "", sFirst_name = "", sLast_name = "", sUser_email = "", sBooked_phone = "", sImg = "", sBilling_address_1 = "";

    CircleImageView iv_profile;
    TextView tvUsername, tvFirstname, tvlastname, tvEmailId, tvPhone, tvAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        click();

        if (Constant.isInternetON(ProfileActivity.this)) {
            callGetDetail();
        } else {
            Constant.InternetDialog(ProfileActivity.this);
        }
    }

    private void callGetDetail() {
        progressLoader.show();
        RequestBody UserIdReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callGetProfile(UserIdReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(ProfileActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);
                    String status = obj.getString("code");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");

                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            sUser_login = jsonObject.getString("user_login");
                            sFirst_name = jsonObject.getString("first_name");
                            sLast_name = jsonObject.getString("last_name");
                            sUser_email = jsonObject.getString("user_email");
                            sBooked_phone = jsonObject.getString("booked_phone");
                            sImg = jsonObject.getString("img");
                            sBilling_address_1 = jsonObject.getString("billing_address_1");
                        }

                        Setdata();
                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(ProfileActivity.this);
                Toast.makeText(ProfileActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void Setdata() {

        Glide.with(ProfileActivity.this)
                .load(sImg)
                .into(iv_profile);

        sUsername = preferenceManager.getString(PreferenceManager.NAME);
        tvUsername.setText(sUsername);
        tvFirstname.setText(sFirst_name);
        tvlastname.setText(sLast_name);
        tvEmailId.setText(sUser_email);
        tvPhone.setText(sBooked_phone);
        tvAddress.setText(sBilling_address_1);
    }

    private void click() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class)
                        .putExtra("username", sUsername)
                        .putExtra("fname", sFirst_name)
                        .putExtra("lname", sLast_name)
                        .putExtra("email", sUser_email)
                        .putExtra("phone", sBooked_phone)
                        .putExtra("address", sBilling_address_1)
                        .putExtra("img", sImg)
                );
                finish();
            }
        });

        llLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferenceManager.clearAll();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.enter, R.anim.exit);
        super.onBackPressed();
    }

    private void init() {
        preferenceManager = new PreferenceManager(ProfileActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        progressLoader = new Constant.ProgressLoader(ProfileActivity.this);
        btnBack = findViewById(R.id.btnBack);
        btnEdit = findViewById(R.id.btnEdit);

        iv_profile = findViewById(R.id.iv_profile);
        tvUsername = findViewById(R.id.tvUsername);
        tvFirstname = findViewById(R.id.tvFirstname);
        tvlastname = findViewById(R.id.tvlastname);
        tvEmailId = findViewById(R.id.tvEmailId);
        tvPhone = findViewById(R.id.tvPhone);
        tvAddress = findViewById(R.id.tvAddress);
        llLogout = findViewById(R.id.llLogout);
    }
}
