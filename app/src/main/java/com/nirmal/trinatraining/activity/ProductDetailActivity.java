package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.SliderAdapterExample;
import com.nirmal.trinatraining.model.mCart;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

import static com.nirmal.trinatraining.activity.EcommerceActivity.ProductTemp;
import static com.nirmal.trinatraining.util.Constant.mCartsList;

public class ProductDetailActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    TextView tvPrice, tvDetail;
    ElegantNumberButton button;
    Button btncart;
    String sCountItem = "1";
    PreferenceManager preferenceManager;
    SliderAdapterExample adapter;
    int sprice;
    mCart mCart;
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);


        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());


        init();
        mCartsList = preferenceManager.getArrayList(PreferenceManager.CART_LIST);
        if (mCartsList != null)
            if (mCartsList.size() > 0) {
                for (int i = 0; i < mCartsList.size(); i++) {
                    if (ProductTemp.getId().equals(mCartsList.get(i).getItemId())) {
                        sCountItem = mCartsList.get(i).getCount();
                        button.setNumber(sCountItem);
                    }
                }
            }


        setData();
        clcik();
        slider();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setData() {
        tvDetail.setText(ProductTemp.getPost_content());
        if (ProductTemp.getReguler_price().isEmpty()) {
            sprice = (Integer.valueOf(ProductTemp.getSale_price()) * Integer.valueOf(sCountItem));
        } else {
            sprice = (Integer.valueOf(ProductTemp.getReguler_price()) * Integer.valueOf(sCountItem));
        }
        tvPrice.setText("$" + String.format("%.2f", Double.valueOf(sprice)));
    }

    private void clcik() {
        button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                sCountItem = button.getNumber();
                if (ProductTemp.getReguler_price().isEmpty()) {
                    sprice = (Integer.valueOf(ProductTemp.getSale_price()) * Integer.valueOf(sCountItem));
                } else {
                    sprice = (Integer.valueOf(ProductTemp.getReguler_price()) * Integer.valueOf(sCountItem));
                }
                tvPrice.setText(sprice + "$");
            }
        });


        btncart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCartsList == null)
                    mCartsList = new ArrayList<>();


                mCart = new mCart(sCountItem, sprice + "", Constant.currentTime(), ProductTemp.getId(), ProductTemp);


                if (mCartsList.size() == 0) {
                    mCartsList.add(mCart);
                } else {
                    for (int i = 0; i < mCartsList.size(); i++) {

                        if (mCartsList.get(i).getItemId().equalsIgnoreCase(ProductTemp.getId())) {
                            mCartsList.set(i, mCart);
                        } else {
                            mCartsList.add(mCart);
                        }
                    }
                }


                preferenceManager.saveArrayList(mCartsList, PreferenceManager.CART_LIST);

                Intent i = new Intent(ProductDetailActivity.this, CartActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void slider() {
        SliderView sliderView = findViewById(R.id.imageSlider);

        adapter = new SliderAdapterExample(this, ProductTemp.getEcommerceSliderList());

        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();
    }

    private void init() {
        preferenceManager = new PreferenceManager(this);
        tvPrice = findViewById(R.id.tvPrice);
        tvDetail = findViewById(R.id.tvDetail);
        btncart = findViewById(R.id.btncart);
        button = findViewById(R.id.number_button);

        tvDetail.setText(ProductTemp.getPost_content());
        if (ProductTemp.getReguler_price().isEmpty()) {
            sprice = (Integer.valueOf(ProductTemp.getSale_price()) * Integer.valueOf(sCountItem));
        } else {
            sprice = (Integer.valueOf(ProductTemp.getReguler_price()) * Integer.valueOf(sCountItem));
        }
        tvPrice.setText(sprice + "$");
    }
}
