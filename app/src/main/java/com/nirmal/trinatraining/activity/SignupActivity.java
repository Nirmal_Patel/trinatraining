package com.nirmal.trinatraining.activity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.hideKeyboard;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "Signup==>>";
    LinearLayout llSignin;
    Constant.ProgressLoader progressLoader;
    int RC_SIGN_IN = 2003;
    Button btnSubmit;
    EditText et_name, et_email, et_phone, et_pws, et_Conf_pws, etFname, etLname;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow();
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//
//
//        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        setContentView(R.layout.activity_signup);


        init();
        click();
    }

    private void click() {
        llSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(SignupActivity.this);

                if (isValid()) {
                    if (Constant.isInternetON(SignupActivity.this)) {
                        CallSignup();
                    } else {
                        Constant.InternetDialog(SignupActivity.this);
                    }
                }
            }
        });
    }

    private void CallSignup() {
        progressLoader.show();
        RequestBody NameReq = RequestBody.create(MediaType.parse("text/plain"), et_name.getText().toString());
        RequestBody EmailReq = RequestBody.create(MediaType.parse("text/plain"), et_email.getText().toString());
        RequestBody PwsReq = RequestBody.create(MediaType.parse("text/plain"), et_pws.getText().toString());
        RequestBody PhoneReq = RequestBody.create(MediaType.parse("text/plain"), et_phone.getText().toString());

        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), etFname.getText().toString());
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), etLname.getText().toString());

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callSignup(NameReq, EmailReq, PwsReq, PhoneReq, firstNameReq, lastNameReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(SignupActivity.this);
                try {
                    if (response.isSuccessful()) {

                        String sResponse = response.body().string();
                        JSONObject obj = new JSONObject(sResponse);

                        String status = obj.getString("code");
                        String sMsg = obj.getString("message");

                        if (status.equals("200")) {
                            String sUserDetail = obj.getString("user_info");
                            JSONObject jsonObject = new JSONObject(sUserDetail);
                            Log.i(TAG, "onResponse: " + jsonObject.toString());

                            Log.i(TAG, "onResponse: " + jsonObject.getString("user_login"));
                            preferenceManager.setString(PreferenceManager.FNAME, jsonObject.getString("first_name"));
                            preferenceManager.setString(PreferenceManager.LNAME, jsonObject.getString("last_name"));

                            preferenceManager.setString(PreferenceManager.NAME, jsonObject.getString("user_login"));
                            preferenceManager.setString(PreferenceManager.EMAIL, jsonObject.getString("email"));
                            preferenceManager.setString(PreferenceManager.PHONE, jsonObject.getString("phone"));
                            preferenceManager.setString(PreferenceManager.PWS, jsonObject.getString("password"));
                            preferenceManager.setString(PreferenceManager.UID, jsonObject.getString("id"));
                            preferenceManager.setBoolean(PreferenceManager.IS_LOGIN, true);

                            CommonDialog(SignupActivity.this, "", sMsg + "",
                                    "OK");

                        }
                        Toast.makeText(SignupActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        CommonDialog(SignupActivity.this, "", jObjError.getString("message") + "",
                                "Cancel");
                    }
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(SignupActivity.this);
                Toast.makeText(SignupActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    private boolean isValid() {
        if (etFname.getText().toString().equals("")) {
            etFname.setError("Enter First Name.");
            etFname.requestFocus();
            return false;
        }
        if (etLname.getText().toString().equals("")) {
            etLname.setError("Enter Last Name.");
            etLname.requestFocus();
            return false;
        }
        if (et_name.getText().toString().equals("")) {
            et_name.setError("Enter Username.");
            et_name.requestFocus();
            return false;
        }
        if (et_email.getText().toString().equals("")) {
            et_email.setError("Enter Email Id.");
            et_email.requestFocus();
            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(et_email.getText().toString()).matches()) {
            et_email.setError("Invalid Email Id.");
            et_email.requestFocus();
            return false;
        }

        if (et_phone.getText().toString().equals("")) {
            et_phone.setError("Enter Phone.");
            et_phone.requestFocus();
            return false;
        }
        if (et_phone.getText().toString().length() != 10) {
            et_phone.setError("Enter Valid Phone.");
            et_phone.requestFocus();
            return false;
        }
        if (et_pws.getText().toString().equals("")) {
            et_pws.setError("Enter Password.");
            et_pws.requestFocus();
            return false;
        }
        if (et_Conf_pws.getText().toString().equals("")) {
            et_Conf_pws.setError("Enter Confirm Password.");
            et_Conf_pws.requestFocus();
            return false;
        }
        if (!et_Conf_pws.getText().toString().equals(et_pws.getText().toString())) {
            et_Conf_pws.setError("Confirm Password Password.");
            et_Conf_pws.requestFocus();
            return false;
        }
        return true;
    }

    private void init() {
        progressLoader = new Constant.ProgressLoader(SignupActivity.this);
        preferenceManager = new PreferenceManager(SignupActivity.this);
        llSignin = findViewById(R.id.llSignin);

        etFname = findViewById(R.id.et_fname);
        etLname = findViewById(R.id.et_lname);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        et_pws = findViewById(R.id.et_pws);
        et_Conf_pws = findViewById(R.id.et_Conf_pws);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    public void CommonDialog(Context context, String sTitle, String sMessage, String btnName) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_common);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        TextView tvMessage = dialog.findViewById(R.id.tvMessage);
        Button btnClose = dialog.findViewById(R.id.btnClose);

        tvTitle.setText("");
        tvMessage.setText(sMessage);
        btnClose.setText(btnName);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnName.equalsIgnoreCase("OK")) {
                    dialog.dismiss();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                    finish();
                } else {
                    dialog.dismiss();
                }
            }
        });
    }
}
