package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.ClickListener;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.EcommerceAdapter;
import com.nirmal.trinatraining.model.ProductItem;
import com.nirmal.trinatraining.model.mCart;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.nirmal.trinatraining.util.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EcommerceActivity extends AppCompatActivity {

    private static final String TAG = "Ecommerce==>";
    public static ProductItem ProductTemp;
    ImageView ivCart;
    RecyclerView rvEcommerce;
    ArrayList<ProductItem> EcommerceList = new ArrayList<>();
    ArrayList<String> EcommerceSliderList = new ArrayList<>();
    ArrayList<mCart> mCartList = new ArrayList<>();
    EcommerceAdapter ecommerceAdapter;
    ProductItem product;
    Constant.ProgressLoader progressLoader;
    PreferenceManager preferenceManager;
    String sId, sPost_title, sPost_content, sPost_date, sImage_url, sReguler_price, sSale_price, gallery_image;
    ImageView btnBack, btnHistory;
    TextView tvCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecommerce);

        init();
        click();

        if (Constant.isInternetON(this)) {
            callProduct();
        } else {
            Constant.InternetDialog(this);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            mCartList = preferenceManager.getArrayList(PreferenceManager.CART_LIST);
            if (mCartList != null && mCartList.size() > 0) {
                if (!mCartList.isEmpty()) {
                    tvCount.setText(mCartList.size() + "");
                } else {
                    tvCount.setText("0");
                }
            } else {
                tvCount.setText("0");
            }
        } catch (Exception e) {
            Log.i(TAG, "onResume: " + e.getMessage());
        }


    }

    private void click() {
        ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EcommerceActivity.this, CartActivity.class));
            }
        });
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EcommerceActivity.this, CriticalHistoryActivity.class)
                        .putExtra("from", "shop"));
            }
        });

        rvEcommerce.addOnItemTouchListener(new RecyclerTouchListener(EcommerceActivity.this, rvEcommerce, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                ProductTemp = EcommerceList.get(position);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(new Intent(EcommerceActivity.this, ProductDetailActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }


    private void init() {
        preferenceManager = new PreferenceManager(EcommerceActivity.this);
        progressLoader = new Constant.ProgressLoader(EcommerceActivity.this);
        btnBack = findViewById(R.id.btnBack);
        btnHistory = findViewById(R.id.btnHistory);
        ivCart = findViewById(R.id.ivCart);
        tvCount = findViewById(R.id.tvCount);
        rvEcommerce = findViewById(R.id.rvEcommerce);
        rvEcommerce.setLayoutManager(new GridLayoutManager(this, 2));
        rvEcommerce.setNestedScrollingEnabled(false);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void callProduct() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callEcomProduct();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(EcommerceActivity.this);
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse:-- " + sResponse);
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("status");
                    String sMsg = obj.getString("massege");

                    if (status.equals("200")) {

                        String sData = obj.getString("data");
                        Log.i(TAG, "onResponse: " + sData);

                        JSONArray jsonArray = new JSONArray(sData);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jObj = jsonArray.getJSONObject(i);


                            sId = jObj.getString("ID");
                            sPost_title = jObj.getString("post_title");
                            sPost_content = jObj.getString("post_content");
                            sPost_date = jObj.getString("post_date");
                            sImage_url = jObj.getString("image_url");
                            sReguler_price = jObj.getString("reguler_price");
                            sSale_price = jObj.getString("sale_price");
                            gallery_image = jObj.getString("gallery_image");
                            JSONArray jsonArray1 = new JSONArray(gallery_image);


                            for (int j = 0; j < jsonArray1.length(); j++) {
                                EcommerceSliderList.add(jsonArray1.getString(j));
                            }


                            product = new ProductItem(sId, sPost_title, sPost_content,
                                    sPost_date, sImage_url, sReguler_price, sSale_price, EcommerceSliderList);
                            EcommerceList.add(product);
                            EcommerceSliderList.clear();

                        }
                        if (!EcommerceList.isEmpty()) {
                            ecommerceAdapter = new EcommerceAdapter(EcommerceActivity.this, EcommerceList);
                            rvEcommerce.setAdapter(ecommerceAdapter);
                        }
                    }
                    //   Toast.makeText(EcommerceActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(EcommerceActivity.this);
                Toast.makeText(EcommerceActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });


    }

}