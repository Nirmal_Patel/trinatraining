package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.activity.EditVideoActivity.videoItemTemp;

public class ShopPaymnetDoneActivity extends AppCompatActivity {

    TextView tvTransId, tvNote;
    Button btnDone;

    String trans_id, from;
    Constant.ProgressLoader progressLoader;
    String sUserId, videoId, sOrderId;

    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_paymnet_done);


        progressLoader = new Constant.ProgressLoader(ShopPaymnetDoneActivity.this);
        preferenceManager = new PreferenceManager(ShopPaymnetDoneActivity.this);
        if (getIntent() != null) {
            trans_id = getIntent().getStringExtra("trans_id");
            from = getIntent().getStringExtra("from");
        }

        tvNote = findViewById(R.id.tvNote);
        tvTransId = findViewById(R.id.tvTransId);
        btnDone = findViewById(R.id.btnDone);


        if (from.equals("subscription")) {
            tvNote.setText("Your Plan is Successfully Active.");
            btnDone.setText("Done");
        } else if (from.equals("editvideo")) {
            btnDone.setText("Go To Upload Video");
            tvNote.setText("Your Order is Successfully Confirm.");
        } else {
            btnDone.setText("Go To Home");
            tvNote.setText("Your Order is Successfully palced.");
        }


        tvTransId.setText("Transaction id: " + trans_id);


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from.equals("subscription")) {
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    Intent myintent = new Intent(ShopPaymnetDoneActivity.this, VideoActivity.class);
                    myintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(myintent);
                    finish();
                } else if (from.equals("editvideo")) {
                    if (Constant.isInternetON(ShopPaymnetDoneActivity.this)) {
                        CallOrder();
                    } else {
                        Constant.InternetDialog(ShopPaymnetDoneActivity.this);
                    }

                } else {
                    onBackPressed();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter, R.anim.exit);
        Intent myintent = new Intent(ShopPaymnetDoneActivity.this, HomeActivity.class);
        myintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myintent);
        finish();
    }


    private void CallOrder() {
        progressLoader.show();
        sUserId = preferenceManager.getString(PreferenceManager.UID);

        videoId = videoItemTemp.getId();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody companyReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody address_1Req = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody address_2Req = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody stateReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody cityReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody postcodeReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody countryReq = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody customer_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody product_idReq = RequestBody.create(MediaType.parse("text/plain"), videoId);
        RequestBody product_qtyReq = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody payment_gatewayReq = RequestBody.create(MediaType.parse("text/plain"), "paypal");
        RequestBody trans_idReq = RequestBody.create(MediaType.parse("text/plain"), trans_id);

        Call<ResponseBody> call = apiService.callOrder(
                first_nameReq, last_nameReq, companyReq, emailReq, phoneReq,
                address_1Req, address_2Req, stateReq, cityReq, postcodeReq, countryReq,
                customer_idReq, product_idReq, product_qtyReq, payment_gatewayReq, trans_idReq
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(ShopPaymnetDoneActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");
                    String sVideo_upload = obj.getString("video_upload");

                    if (status.equals("200")) {
                        String sData = obj.getString("data");
                        JSONArray jsonArray = new JSONArray(sData);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            sOrderId = jsonObject.getString("id");
                        }

                        startActivity(new Intent(ShopPaymnetDoneActivity.this, VideoUpDownActivity.class).putExtra("oder_id", sOrderId));
                        finish();
                    }
                    Toast.makeText(ShopPaymnetDoneActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(ShopPaymnetDoneActivity.this);
                Toast.makeText(ShopPaymnetDoneActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }
}
