package com.nirmal.trinatraining.activity;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.SelectedListner;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.adapter.CartAdapert;
import com.nirmal.trinatraining.model.mCart;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nirmal.trinatraining.util.Constant.addresses;
import static com.nirmal.trinatraining.util.Constant.getLatLong2Address;
import static com.nirmal.trinatraining.util.Constant.mCartsList;


public class CartActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final String TAG = "Cart==>>";
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constant.PAYPAL_CLIENT_ID)
            .merchantName("TrinaTraining");
    String tansID = "";
    PayPalPayment thingToBuy;
    RecyclerView rvProductList;
    CartAdapert cartAdapert;
    PreferenceManager preferenceManager;
    Button btnPaymnet;
    TextView tvTotalItem, tvTotalPrice, tvEmptyCart;
    mCart mCart;
    int sTotalItem = 0, sTotalPrice = 0;
    Constant.ProgressLoader progressLoader;
    String sUserId;
    EditText etFName, etLastName, etCompany, etEmail, etContact, etAdd1, etAdd2,
            etState, etCity, etCountry, etPinCode;

    StringBuilder csvBuilder = new StringBuilder();
    String csv = "";
    ArrayList<String> ids = new ArrayList<>();
    ImageView btnBack;
    private String SEPARATOR = ",";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);


        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> onBackPressed());

        init();
    }

    private void init() {
        preferenceManager = new PreferenceManager(this);
        progressLoader = new Constant.ProgressLoader(CartActivity.this);
        sUserId = preferenceManager.getString(PreferenceManager.UID);
        btnPaymnet = findViewById(R.id.btnPaymnet);
        tvTotalItem = findViewById(R.id.tvTotalItem);
        tvTotalPrice = findViewById(R.id.tvTotalPrice);
        tvEmptyCart = findViewById(R.id.tvEmptyyCart);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        Constant.mCartsList = preferenceManager.getArrayList(PreferenceManager.CART_LIST);
        hideShowLayout();

        try {
            if (preferenceManager.getArrayList(PreferenceManager.CART_LIST) != null) {
                Constant.mCartsList = preferenceManager.getArrayList(PreferenceManager.CART_LIST);
            }
        } catch (Exception e) {
            Log.i(TAG, "init: ");
        }

        CountTotal();

        rvProductList = findViewById(R.id.rvProductList);
        rvProductList.setLayoutManager(new LinearLayoutManager(this));
        rvProductList.setNestedScrollingEnabled(false);


        cartAdapert = new CartAdapert(CartActivity.this, Constant.mCartsList, new SelectedListner() {
            @Override
            public void onCountClick(int pos, String sCountItem, String sprice) {
                mCart = new mCart(sCountItem, sprice + "", Constant.currentTime(),
                        mCartsList.get(pos).getProduct().getId(), mCartsList.get(pos).getProduct());
                Constant.mCartsList.set(pos, mCart);
                CountTotal();
            }

            @Override
            public void onDeleteClick(int pos) {

                dialogDelteItem(pos);
            }
        });
        rvProductList.setAdapter(cartAdapert);


        btnPaymnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Constant.mCartsList.isEmpty()) {
                    for (int i = 0; i < mCartsList.size(); i++) {
                        if (ids.size() > 0) {
                            for (int j = 0; j < ids.size(); j++) {
                                if (!mCartsList.get(i).getItemId().equals(ids.get(j))) {
                                    ids.add(mCartsList.get(i).getItemId());
                                }
                            }
                        } else {
                            ids.add(mCartsList.get(i).getItemId());
                        }
                    }

                    for (String city : ids) {
                        csvBuilder.append(city);
                        csvBuilder.append(SEPARATOR);
                    }
                    csv = csvBuilder.toString();
                    try {
                        csv = csv.substring(0, csv.length() - SEPARATOR.length());
                    } catch (Exception e) {
                        Log.e("DATAT", e.getMessage());
                    }

                    Log.i(TAG, "onClick: " + csv);
                    if (!csv.equals("")) {
                        Log.i(TAG, "onClick: " + sTotalPrice);
                        proceedPayment(sTotalPrice + "", "Trina training");
                    }
                }
            }

        });

    }


    private void proceedPayment(String price, String product) {
        thingToBuy = new PayPalPayment(new BigDecimal(price), "USD", product, PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private void CountTotal() {
        sTotalItem = 0;
        sTotalPrice = 0;
        for (int i = 0; i < Constant.mCartsList.size(); i++) {
            sTotalItem = (sTotalItem + Integer.parseInt(Constant.mCartsList.get(i).getCount()));
            sTotalPrice = (sTotalPrice + Integer.parseInt(Constant.mCartsList.get(i).getPrice()));
        }
        tvTotalItem.setText("Total Item : " + sTotalItem + "");
        tvTotalPrice.setText("Total Price : " + "$" + String.format("%.02f", Double.valueOf(sTotalPrice)) + "");
    }

    private void showDetailDialog() {
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.raw_detail_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }

        etFName = dialog.findViewById(R.id.etFirstName);
        etLastName = dialog.findViewById(R.id.etLastName);
        etCompany = dialog.findViewById(R.id.etCompany);
        etEmail = dialog.findViewById(R.id.etEmail);
        etContact = dialog.findViewById(R.id.etContact);
        etAdd1 = dialog.findViewById(R.id.etAdd1);
        etAdd2 = dialog.findViewById(R.id.etAdd2);
        etState = dialog.findViewById(R.id.etState);
        etCity = dialog.findViewById(R.id.etCity);
        etCountry = dialog.findViewById(R.id.etCountry);
        etPinCode = dialog.findViewById(R.id.etPinCode);
        TextView tvNext = dialog.findViewById(R.id.tvNext);

        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        if (!addresses.isEmpty()) {
            etCity.setText(addresses.get(0).getLocality());
            etState.setText(addresses.get(0).getAdminArea());
            etCountry.setText(addresses.get(0).getCountryName());
            etPinCode.setText(addresses.get(0).getPostalCode());
        } else {
            getLatLong2Address(CartActivity.this);
        }

        tvNext.setOnClickListener(view -> {
            if (etFName.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "First Name is Required", Toast.LENGTH_LONG).show();
            } else if (etLastName.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Last Name is Required", Toast.LENGTH_LONG).show();
            } else if (etCompany.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Company Name is Required", Toast.LENGTH_LONG).show();
            } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Email is Required", Toast.LENGTH_LONG).show();
            } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Email is Required", Toast.LENGTH_LONG).show();
            } else if (etContact.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Contact No. is Required", Toast.LENGTH_LONG).show();
            } else if (etAdd1.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Address is Required", Toast.LENGTH_LONG).show();
            } else if (etCity.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "City is Required", Toast.LENGTH_LONG).show();
            } else if (etState.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "State is Required", Toast.LENGTH_LONG).show();
            } else if (etCountry.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Country is Required", Toast.LENGTH_LONG).show();
            } else if (etPinCode.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(CartActivity.this, "Postal Code is Required", Toast.LENGTH_LONG).show();
            } else {

                if (Constant.isInternetON(CartActivity.this)) {
                    CallOrder();
                } else {
                    Constant.InternetDialog(CartActivity.this);
                }
                dialog.dismiss();
            }
        });
    }

    private void CallOrder() {
        progressLoader.show();
        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);

        RequestBody first_nameReq = RequestBody.create(MediaType.parse("text/plain"), etFName.getText().toString());
        RequestBody last_nameReq = RequestBody.create(MediaType.parse("text/plain"), etLastName.getText().toString());
        RequestBody companyReq = RequestBody.create(MediaType.parse("text/plain"), etCompany.getText().toString());
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), etEmail.getText().toString());
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), etContact.getText().toString());
        RequestBody address_1Req = RequestBody.create(MediaType.parse("text/plain"), etAdd1.getText().toString());
        RequestBody address_2Req = RequestBody.create(MediaType.parse("text/plain"), etAdd2.getText().toString());
        RequestBody stateReq = RequestBody.create(MediaType.parse("text/plain"), etState.getText().toString());
        RequestBody cityReq = RequestBody.create(MediaType.parse("text/plain"), etCity.getText().toString());
        RequestBody postcodeReq = RequestBody.create(MediaType.parse("text/plain"), etPinCode.getText().toString());
        RequestBody countryReq = RequestBody.create(MediaType.parse("text/plain"), etCountry.getText().toString());
        RequestBody customer_idReq = RequestBody.create(MediaType.parse("text/plain"), sUserId);
        RequestBody product_idReq = RequestBody.create(MediaType.parse("text/plain"), csv);
        RequestBody product_qtyReq = RequestBody.create(MediaType.parse("text/plain"), sTotalItem + "");
        RequestBody payment_gatewayReq = RequestBody.create(MediaType.parse("text/plain"), "paypal");
        RequestBody trans_idReq = RequestBody.create(MediaType.parse("text/plain"), tansID);

        Call<ResponseBody> call = apiService.callOrder(
                first_nameReq, last_nameReq, companyReq, emailReq, phoneReq,
                address_1Req, address_2Req, stateReq, cityReq, postcodeReq, countryReq,
                customer_idReq, product_idReq, product_qtyReq, payment_gatewayReq, trans_idReq
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(CartActivity.this);
                try {

                    //TODO OPen Trans  Detail Page...  Then Ok To Home Screen

                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (!Constant.mCartsList.isEmpty()) {
                        Constant.mCartsList.clear();
                    }
                    preferenceManager.saveArrayList(Constant.mCartsList, PreferenceManager.CART_LIST);

                    startActivity(new Intent(CartActivity.this, ShopPaymnetDoneActivity.class)
                            .putExtra("trans_id", tansID)
                            .putExtra("from", "shop")
                    );
                    finish();
                    //  Toast.makeText(CartActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    try {
                        String sResponseErr = response.errorBody().string();
                        Log.e("Ex:>>", sResponseErr);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        Log.e("Ex:>>", e1.getMessage());
                    }
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(CartActivity.this);
                Toast.makeText(CartActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        JSONObject jsonObject = confirm.toJSONObject();
                        tansID = jsonObject.getJSONObject("response").getString("id");
                        Log.e("tansID", "tansID ===> " + tansID);

                        showDetailDialog();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onActivityResult: " + e.getMessage());
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i(TAG, auth.toJSONObject().toString(4));
                        Log.i(TAG, auth.getAuthorizationCode());
                        Toast.makeText(getApplicationContext(), "Future Payment code received from PayPal", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Log.i("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }


    private void hideShowLayout() {
        if (mCartsList == null || mCartsList.size() == 0) {

            tvEmptyCart.setVisibility(View.VISIBLE);
        } else {
            tvEmptyCart.setVisibility(View.GONE);
        }

    }

    private void dialogDelteItem(int pos) {
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dailog_delete_item);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Constant.mCartsList.remove(pos);
                preferenceManager.saveArrayList(Constant.mCartsList, PreferenceManager.CART_LIST);
                CountTotal();
                cartAdapert.notifyDataSetChanged();
            }
        });

    }
}
