package com.nirmal.trinatraining.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity {


    private static final String TAG = "Forgot==>>";
    PreferenceManager preferenceManager;
    Constant.ProgressLoader progressLoader;
    EditText et_email;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_forgot);

        init();
        click();

    }

    private void click() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    if (Constant.isInternetON(ForgotActivity.this)) {
                        callForgot();
                    } else {
                        Constant.InternetDialog(ForgotActivity.this);
                    }
                }
            }
        });
    }

    private void callForgot() {
        progressLoader.show();
        RequestBody EmailReq = RequestBody.create(MediaType.parse("text/plain"), et_email.getText().toString());

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        Call<ResponseBody> call = apiService.callForgot(EmailReq);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(ForgotActivity.this);
                try {
                    String sResponse = response.body().string();
                    JSONObject obj = new JSONObject(sResponse);

                    String status = obj.getString("code");
                    String sMsg = obj.getString("message");

                    if (status.equals("200")) {
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        startActivity(new Intent(ForgotActivity.this, LoginActivity.class));
                        finish();
                    }
                    Toast.makeText(ForgotActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(ForgotActivity.this);
                Toast.makeText(ForgotActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }

    private boolean isValid() {
        if (et_email.getText().toString().equals("")) {
            et_email.setError("Enter Email Id.");
            et_email.requestFocus();
            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(et_email.getText().toString()).matches()) {
            et_email.setError("Invalid Email Id.");
            et_email.requestFocus();
            return false;
        }

        return true;
    }

    private void init() {
        progressLoader = new Constant.ProgressLoader(ForgotActivity.this);
        preferenceManager = new PreferenceManager(ForgotActivity.this);

        btnSubmit = findViewById(R.id.btnSubmit);
        et_email = findViewById(R.id.et_email);
    }
}
