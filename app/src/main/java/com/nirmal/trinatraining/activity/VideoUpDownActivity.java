package com.nirmal.trinatraining.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.Network.ApiClient;
import com.nirmal.trinatraining.Network.ApiInterface;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.util.Constant;
import com.nirmal.trinatraining.util.PreferenceManager;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoUpDownActivity extends AppCompatActivity {

    private static final String TAG = "Video==>>";
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1004;
    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 3001;
    Constant.ProgressLoader progressLoader;
    PreferenceManager preferenceManager;
    TextView tvUploadVideo;
    ImageView ivVideo;
    LinearLayout llSuccess, llUpload;
    File file_1 = null;
    RequestBody mRequBody_1 = null;
    MultipartBody.Part fileToUpload_1 = null;
    String sOrderId;
    Button btnLogin;

    ImageView ivBg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_video_layout);
        if (getIntent() != null) {
            sOrderId = getIntent().getStringExtra("oder_id");
        }
        init();
    }

    private void VideoUpload() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("video/*");
            //       intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
        }
    }

    private void init() {
        preferenceManager = new PreferenceManager(VideoUpDownActivity.this);
        progressLoader = new Constant.ProgressLoader(VideoUpDownActivity.this);
        ivVideo = findViewById(R.id.ivVideo);
        btnLogin = findViewById(R.id.btnLogin);
        llSuccess = findViewById(R.id.llSuccess);
        llUpload = findViewById(R.id.llUpload);
        tvUploadVideo = findViewById(R.id.tvUploadVideo);
        ivBg = findViewById(R.id.ivBg);

        tvUploadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VideoUpload();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VideoUpDownActivity.this, CriticalHistoryActivity.class).putExtra("from", "critical")
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
    }

    private void VideoUploadApi(String path) {
        progressLoader.show();
        file_1 = new File(path);
        if (file_1.isFile() && file_1.exists()) {
            mRequBody_1 = RequestBody.create(MediaType.parse("multipart/form-data"), file_1);
            fileToUpload_1 = MultipartBody.Part.createFormData("my_image_upload", file_1.getName(), mRequBody_1);
        }

        ApiInterface apiService = ApiClient.getClient("1").create(ApiInterface.class);
        RequestBody order_idReq = RequestBody.create(MediaType.parse("text/plain"), sOrderId);
        RequestBody video_descReq = RequestBody.create(MediaType.parse("text/plain"), "XYZ");

        Call<ResponseBody> call = apiService.callVideoUpload(order_idReq, video_descReq, fileToUpload_1);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressLoader.dismiss(VideoUpDownActivity.this);
                try {
                    String sResponse = response.body().string();
                    Log.i(TAG, "onResponse: ");
                    JSONObject obj = new JSONObject(sResponse);
                    String status = obj.getString("code");
                    String sMsg = obj.getString("massege");
                    if (status.equals("200")) {

                        llUpload.setVisibility(View.GONE);
                        llSuccess.setVisibility(View.VISIBLE);

                    }
                    Toast.makeText(VideoUpDownActivity.this, sMsg + "", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("Ex:>>", e.getMessage());
                    e.printStackTrace();
                }
                progressLoader.Isshow();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressLoader.dismiss(VideoUpDownActivity.this);
                Toast.makeText(VideoUpDownActivity.this, call.toString() + "", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                ivBg.setVisibility(View.GONE);
                Uri selectedImageUri = data.getData();
                String filemanagerstring = selectedImageUri.getPath();
                //String path = getPath(selectedImageUri);
                String path = getRealPathFromURI(selectedImageUri);
                if (path != null) {
                    Glide.with(this).load(selectedImageUri).into(ivVideo);
                    if (Constant.isInternetON(VideoUpDownActivity.this)) {
                        Log.e("uriPath", "uriPath  --> " + path + "filemanagerstring---> " + filemanagerstring);
                        VideoUploadApi(path);
                    } else {
                        Constant.InternetDialog(VideoUpDownActivity.this);
                    }
                }
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPath(Uri uri) {
        String path = "";
        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = this.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Video.Media._ID + " = ? ", new String[]{document_id}, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}
