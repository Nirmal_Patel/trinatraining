package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.ItemListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.model.ServiceListItem;

import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    Context context;
    List<ServiceListItem> serviceList;
    ItemListner listner;

    public ServiceAdapter(Context context, List<ServiceListItem> serviceList, ItemListner listner) {
        this.context = context;
        this.serviceList = serviceList;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_service, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        ServiceListItem serviceListItem = serviceList.get(position);

        holder.btnService.setText(serviceListItem.getTitle());
        if (serviceListItem.isSelected()) {
            holder.btnService.setBackgroundResource(R.drawable.btn_border_select);
            holder.btnService.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.btnService.setBackgroundResource(R.drawable.btn_border);
            holder.btnService.setTextColor(Color.parseColor("#8F66EC"));
        }

        holder.btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onSelected(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        Button btnService;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            btnService = itemView.findViewById(R.id.btnService);
        }
    }
}
