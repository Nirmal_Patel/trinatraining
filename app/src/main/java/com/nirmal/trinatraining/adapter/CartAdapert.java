package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.nirmal.trinatraining.InterFace.SelectedListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.CartActivity;
import com.nirmal.trinatraining.model.mCart;

import java.util.ArrayList;


public class CartAdapert extends RecyclerView.Adapter<CartAdapert.ViewHolder> {

    int sprice;
    String sCountItem;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<mCart> productList = new ArrayList<>();
    private SelectedListner listner;

    public CartAdapert(CartActivity cartActivity, ArrayList<mCart> productList, SelectedListner listner) {
        this.context = cartActivity;
        this.productList = productList;
        this.inflater = LayoutInflater.from(cartActivity);
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_cart, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mCart product = productList.get(position);
        Glide.with(context).load(product.getProduct().getImage_url()).into(holder.ivImage);
        holder.tvTitle.setText(product.getProduct().getPost_title());
        holder.tvDate.setText(product.getProduct().getPost_content());
        holder.tv_price.setText("$" + String.format("%.2f", Double.valueOf(product.getPrice())));
        holder.number_button.setNumber(product.getCount());
        holder.number_button.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                sCountItem = holder.number_button.getNumber();
                if (!product.getProduct().getReguler_price().equals("")) {
                    sprice = (Integer.valueOf(product.getProduct().getReguler_price()) * Integer.valueOf(sCountItem));
                } else {
                    sprice = (Integer.valueOf(product.getProduct().getSale_price()) * Integer.valueOf(sCountItem));
                }
                holder.tv_price.setText("$" + String.format("%.02f", Double.valueOf(sprice)));
                listner.onCountClick(position, sCountItem, sprice + "");
            }
        });

        holder.ivDelete.setOnClickListener(view -> listner.onDeleteClick(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;
        ImageView ivDelete;
        TextView tvTitle;
        TextView tvDate;
        TextView tv_price;
        ElegantNumberButton number_button;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tv_price = itemView.findViewById(R.id.tv_price);
            number_button = itemView.findViewById(R.id.number_button);
        }
    }

}
