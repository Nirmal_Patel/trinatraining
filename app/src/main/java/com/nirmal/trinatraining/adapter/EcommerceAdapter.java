package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.EcommerceActivity;
import com.nirmal.trinatraining.model.ProductItem;

import java.util.ArrayList;

public class EcommerceAdapter extends RecyclerView.Adapter<EcommerceAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<ProductItem> videoItemList = new ArrayList<>();

    public EcommerceAdapter(EcommerceActivity ecommerceActivity, ArrayList<ProductItem> videoItemList) {
        this.context = ecommerceActivity;
        this.videoItemList = videoItemList;
        this.inflater = LayoutInflater.from(ecommerceActivity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_ecommerce, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvTitle.setText(videoItemList.get(position).getPost_title());

        if (videoItemList.get(position).getSale_price().trim().isEmpty()) {
            holder.rvSale.setVisibility(View.GONE);
            holder.tvPrice.setText("$" + String.format("%.2f", Double.valueOf(videoItemList.get(position).getReguler_price())));
        } else {
            holder.rvSale.setVisibility(View.GONE);
            holder.tvPrice.setText("$" + String.format("%.2f", Double.valueOf(videoItemList.get(position).getSale_price())));
        }

        Glide.with(context).load(videoItemList.get(position).getImage_url()).into(holder.ivProduct);
    }

    @Override
    public int getItemCount() {
        return videoItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvPrice;
        RelativeLayout rvSale;
        ImageView ivProduct;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            rvSale = itemView.findViewById(R.id.rvSale);
            ivProduct = itemView.findViewById(R.id.ivProduct);
        }
    }

}
