package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.SelectedListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.OrderHistoryActivity;
import com.nirmal.trinatraining.model.OrderHistory;

import java.util.ArrayList;

import static com.nirmal.trinatraining.util.Constant.parseDateToddMMyyyy;


public class OrderHistorAdapert extends RecyclerView.Adapter<OrderHistorAdapert.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<OrderHistory> historyList = new ArrayList<>();
    private SelectedListner listner;


    public OrderHistorAdapert(OrderHistoryActivity orderHistoryActivity, ArrayList<OrderHistory> historyList) {
        this.context = orderHistoryActivity;
        this.historyList = historyList;
        this.inflater = LayoutInflater.from(orderHistoryActivity);
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        double price = Double.parseDouble(historyList.get(position).getInitial_payment());
        holder.tvTitle.setText(historyList.get(position).getName());
        holder.TvPrice.setText("$" + String.format("%.2f", price));
        holder.tvStatus.setText(historyList.get(position).getStatus());
        holder.tvStartDate.setText("Start Date : " + parseDateToddMMyyyy(historyList.get(position).getStartdate()));
        holder.tvEndDate.setText("End Date : " + parseDateToddMMyyyy(historyList.get(position).getEnddate()));
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvStatus, tvStartDate, tvEndDate, TvPrice;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvStartDate = itemView.findViewById(R.id.tvStartDate);
            tvEndDate = itemView.findViewById(R.id.tvEndDate);
            TvPrice = itemView.findViewById(R.id.tvPrice);
        }
    }

}
