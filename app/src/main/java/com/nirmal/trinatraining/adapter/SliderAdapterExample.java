package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

import static com.nirmal.trinatraining.activity.EcommerceActivity.ProductTemp;


public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

    private Context context;
    private ArrayList<String> stringArrayList = new ArrayList<>();

    public SliderAdapterExample(Context context, ArrayList<String> ecommerceSliderList) {
        this.context = context;
        this.stringArrayList = ecommerceSliderList;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_slider_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        viewHolder.textViewDescription.setText("This is slider item " + position);

        if (!stringArrayList.isEmpty()) {
            Glide.with(viewHolder.itemView)
                    .load(stringArrayList.get(position))
                    .into(viewHolder.imageViewBackground);
        } else {
            Glide.with(viewHolder.itemView)
                    .load(ProductTemp.getImage_url().toString())
                    .into(viewHolder.imageViewBackground);
        }
    }

    @Override
    public int getCount() {
        if (stringArrayList.isEmpty()) {
            return 1;
        } else {
            return stringArrayList.size();
        }

    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}