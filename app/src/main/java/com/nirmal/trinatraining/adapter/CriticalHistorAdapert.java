package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.SelectedListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.CriticalHistoryActivity;
import com.nirmal.trinatraining.model.CriticalOrderHistory;

import java.util.ArrayList;

import static com.nirmal.trinatraining.activity.CriticalHistoryActivity.from;
import static com.nirmal.trinatraining.util.Constant.parseDateToddMMyyyy;


public class CriticalHistorAdapert extends RecyclerView.Adapter<CriticalHistorAdapert.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<CriticalOrderHistory> historyList = new ArrayList<>();
    private SelectedListner listner;


    public CriticalHistorAdapert(CriticalHistoryActivity orderHistoryActivity, ArrayList<CriticalOrderHistory> historyList) {
        this.context = orderHistoryActivity;
        this.historyList = historyList;
        this.inflater = LayoutInflater.from(orderHistoryActivity);
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_history_critical, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        double price = Double.parseDouble(historyList.get(position).getInitial_payment());
        Log.i("nirmal1", "onBindViewHolder: " + String.format("Value of a: %.2f", price));

        holder.tvOrderId.setText("Order Id #" + historyList.get(position).getOrder_key());
        holder.tvTitle.setText("Transaction Id : " + historyList.get(position).getSpayment_transaction_id());
        holder.TvPrice.setText("$" + String.format("%.2f", price));
        holder.tvStatus.setText(historyList.get(position).getStatus());
        holder.tvStartDate.setText("Date : " + parseDateToddMMyyyy(historyList.get(position).getStartdate()));


        if (!from.equalsIgnoreCase("shop")) {
            if (historyList.get(position).getVideo_service_id().equals("")) {
                holder.tvStatus.setText("Upload");
            } else {
                holder.tvStatus.setText("Check");
            }
        }

    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvOrderId, tvTitle, tvStatus, tvStartDate, tvEndDate, TvPrice;


        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvStartDate = itemView.findViewById(R.id.tvStartDate);
            tvEndDate = itemView.findViewById(R.id.tvEndDate);
            TvPrice = itemView.findViewById(R.id.tvPrice);
        }
    }

}
