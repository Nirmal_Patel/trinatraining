package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.MultiCLickListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.AppoinmentListActivity;
import com.nirmal.trinatraining.model.getService.GetBooking;

import java.util.ArrayList;
import java.util.Date;

import static com.nirmal.trinatraining.util.Constant.parseDateToddMMyyyy;


public class AppoinmentAdapter extends RecyclerView.Adapter<AppoinmentAdapter.ViewHolder> {

    Date date;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<GetBooking> videoList = new ArrayList<>();
    private MultiCLickListner listner;

    public AppoinmentAdapter(AppoinmentListActivity appoinmentListActivity, ArrayList<GetBooking> appoinmentList, MultiCLickListner listner) {
        this.context = appoinmentListActivity;
        this.videoList = appoinmentList;
        this.inflater = LayoutInflater.from(appoinmentListActivity);
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_appoinment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.tvTitle.setText(videoList.get(position).getTitle());
        holder.tvDate.setText(parseDateToddMMyyyy(videoList.get(position).getStart_date()));
        holder.tvPrice.setText("Price: $" + String.format("%.2f", Double.valueOf(videoList.get(position).getPrice())));
        holder.tvDuration.setText("Duration: " + videoList.get(position).getDuration());
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onDeleteCLicked(position);
            }
        });
        holder.llUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onUpdateClicked(position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvDate, tvPrice, tvDuration;
        LinearLayout llDelete, llUpdate;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDuration = itemView.findViewById(R.id.tvDuration);
            llUpdate = itemView.findViewById(R.id.llUpdate);
            llDelete = itemView.findViewById(R.id.llDelete);
        }
    }

}
