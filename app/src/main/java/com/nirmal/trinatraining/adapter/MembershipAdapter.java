package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.VideoActivity;
import com.nirmal.trinatraining.model.mMembershipType;
import com.nirmal.trinatraining.util.PreferenceManager;

import java.util.ArrayList;

public class MembershipAdapter extends RecyclerView.Adapter<MembershipAdapter.ViewHolder> {

    //private int[] myImageList = new int[]{R.drawable.iv_bronze, R.drawable.iv_silver, R.drawable.iv_gold};
    //private int[] myImageList = new int[]{R.drawable.iv_gold};
    PreferenceManager preferenceManager;
    String isActive = "";
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<mMembershipType> mMembershipTypeArrayList = new ArrayList<>();

    public MembershipAdapter(VideoActivity videoActivity, ArrayList<mMembershipType> mMembershipTypeArrayList) {
        this.context = videoActivity;
        this.mMembershipTypeArrayList = mMembershipTypeArrayList;
        this.inflater = LayoutInflater.from(videoActivity);
        preferenceManager = new PreferenceManager(videoActivity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_membership, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        isActive = preferenceManager.getString(PreferenceManager.VIDEO_MEMBERSHIP);
        holder.ivIcon.setImageResource(R.drawable.home_sub);
        holder.tvPlanTitle.setText(mMembershipTypeArrayList.get(position).getsName());

        double amountDoub = Double.valueOf(mMembershipTypeArrayList.get(position).getsInitial_payment());

        String month = mMembershipTypeArrayList.get(position).getsExpiration_number();
        String DayMonth = mMembershipTypeArrayList.get(position).getSexpiration_period();
        holder.tvPlan.setText("Plan : $" + String.format("%.2f", amountDoub) + " / " + month + " " + DayMonth);
        holder.tvExp.setText("Expiration : $ 2 / 1 Day");

        if (isActive.equals("active")) {
            holder.btnActive.setText("Cancel Plan");
        } else {
            holder.btnActive.setText("Subscribe Now");
        }


/*        holder.btnActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mMembershipTypeArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivIcon;
        TextView tvPlanTitle, tvPlan, tvExp, btnActive;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvPlanTitle = itemView.findViewById(R.id.tvPlanTitle);
            tvPlan = itemView.findViewById(R.id.tvPlan);
            tvExp = itemView.findViewById(R.id.tvExp);
            btnActive = itemView.findViewById(R.id.btnActive);
        }
    }
}
