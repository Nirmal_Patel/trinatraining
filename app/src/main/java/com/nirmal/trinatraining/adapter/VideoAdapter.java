package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.VideoActivity;
import com.nirmal.trinatraining.model.Vedio;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Vedio> videoList = new ArrayList<>();

    public VideoAdapter(VideoActivity videoActivity, ArrayList<Vedio> videoList) {
        this.context = videoActivity;
        this.videoList = videoList;
        this.inflater = LayoutInflater.from(videoActivity);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_video, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvTitle.setText(videoList.get(position).getPost_title());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.dm_a);
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(videoList.get(position).getImage_url())
                .into(holder.ivVideo);


    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvContain;
        ImageView ivVideo;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvContain = itemView.findViewById(R.id.tvContain);
            ivVideo = itemView.findViewById(R.id.ivVideo);
        }
    }
}
