package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nirmal.trinatraining.InterFace.ItemListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.activity.EditVideoActivity;
import com.nirmal.trinatraining.model.VideoItem;

import java.util.ArrayList;

public class VideoEditAdapter extends RecyclerView.Adapter<VideoEditAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<VideoItem> videoItemList = new ArrayList<>();

    private ItemListner listner;

    public VideoEditAdapter(EditVideoActivity editingActivity, ArrayList<VideoItem> videoItemList, ItemListner listner) {
        this.context = editingActivity;
        this.videoItemList = videoItemList;
        this.inflater = LayoutInflater.from(editingActivity);
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(this.inflater.inflate(R.layout.adapter_video_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (videoItemList.get(position).getPost_title().equals("")) {
            holder.tvTitle.setText("");
        } else {
            holder.tvTitle.setText(videoItemList.get(position).getPost_title());
        }

        if (videoItemList.get(position).getPost_content().equals("")) {
            holder.tvDescription.setText("");
        } else {
            holder.tvDescription.setText(videoItemList.get(position).getPost_content());
        }

        if (videoItemList.get(position).getReguler_price().trim().isEmpty()) {
            holder.tvPrice.setText("$" + String.format("%.2f", Double.valueOf(videoItemList.get(position).getSale_price())));

        } else {
            holder.tvPrice.setText("$" + String.format("%.2f", Double.valueOf(videoItemList.get(position).getReguler_price())));
        }

        /*if (videoItemList.get(position).getSale_price().trim().isEmpty()){
            holder.rvSale.setVisibility(View.GONE);
        }else {
            holder.rvSale.setVisibility(View.VISIBLE);
        }*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onSelected(position);
            }
        });


        Glide.with(context).load(videoItemList.get(position).getImage_url()).into(holder.ivProduct);
    }

    @Override
    public int getItemCount() {
        return videoItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvPrice, tvDescription;
        //RelativeLayout rvSale;
        ImageView ivProduct;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            //rvSale =itemView.findViewById(R.id.rvSale);
            ivProduct = itemView.findViewById(R.id.ivImage);
        }
    }

}
