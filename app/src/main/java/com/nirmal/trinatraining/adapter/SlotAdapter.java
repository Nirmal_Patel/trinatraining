package com.nirmal.trinatraining.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nirmal.trinatraining.InterFace.ItemListner;
import com.nirmal.trinatraining.R;
import com.nirmal.trinatraining.model.SlotListItem;

import java.util.List;

public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.MyViewHolder> {


    List<SlotListItem> slotList;
    Context context;
    ItemListner listner;

    public SlotAdapter(List<SlotListItem> slotList, Context context, ItemListner listner) {
        this.slotList = slotList;
        this.context = context;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_slot_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.btnTImeSlot.setText(slotList.get(position).getTime());
        if (slotList.get(position).isSelected()) {

            holder.btnTImeSlot.setBackgroundResource(R.drawable.btn_border_select);
            holder.btnTImeSlot.setTextColor(Color.parseColor("#ffffff"));

        } else {

            holder.btnTImeSlot.setBackgroundResource(R.drawable.btn_border);
            holder.btnTImeSlot.setTextColor(Color.parseColor("#8F66EC"));

        }
        holder.btnTImeSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listner.onSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        Button btnTImeSlot;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            btnTImeSlot = itemView.findViewById(R.id.btnTImeSlot);

        }
    }
}
