package com.nirmal.trinatraining;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.danikula.videocache.HttpProxyCacheServer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nirmal.trinatraining.util.PreferenceManager;

public class App extends Application {

    private static final String TAG = "App";
    public static Context appContext;
    public static HttpProxyCacheServer cacheServer;
    public static String sToken = "";
    static App application;
    PreferenceManager preferenceManager;

    public static Context getAppContext() {
        return appContext;
    }

    public static HttpProxyCacheServer getCacheServer(Context context) {
        if (application.cacheServer == null)
            application.cacheServer = application.buildHttpCacheServer();
        return application.cacheServer;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    private HttpProxyCacheServer buildHttpCacheServer() {
        return new HttpProxyCacheServer.Builder(this)
                .cacheDirectory(getCacheDir())
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        application = this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        sToken = FirebaseToken();

        preferenceManager = new PreferenceManager(appContext);
        preferenceManager.setString(PreferenceManager.TOKEN, sToken);
    }

    private String FirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.i(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        String token = task.getResult().getToken();
                        sToken = token;
                        Log.i(TAG, "onComplete: " + sToken);
                        preferenceManager.setString(PreferenceManager.DEVICE_ID, sToken);
                        Log.i(TAG, token);
                    }
                });

        return sToken;

    }
}
