package com.nirmal.trinatraining.model;

public class OrderHistory {

    private String startdate;
    private String enddate;
    private String name;
    private String status;
    private String subscription_transaction_id;
    private String payment_transaction_id;
    private String initial_payment;

    public OrderHistory(String startdate, String enddate, String name, String status, String subscription_transaction_id,
                        String payment_transaction_id, String initial_payment) {
        this.startdate = startdate;
        this.enddate = enddate;
        this.name = name;
        this.status = status;
        this.subscription_transaction_id = subscription_transaction_id;
        this.payment_transaction_id = payment_transaction_id;
        this.initial_payment = initial_payment;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubscription_transaction_id() {
        return subscription_transaction_id;
    }

    public void setSubscription_transaction_id(String subscription_transaction_id) {
        this.subscription_transaction_id = subscription_transaction_id;
    }

    public String getPayment_transaction_id() {
        return payment_transaction_id;
    }

    public void setPayment_transaction_id(String payment_transaction_id) {
        this.payment_transaction_id = payment_transaction_id;
    }

    public String getInitial_payment() {
        return initial_payment;
    }

    public void setInitial_payment(String initial_payment) {
        this.initial_payment = initial_payment;
    }
}
