package com.nirmal.trinatraining.model.getService;

import java.io.Serializable;

public class ResponseDTO implements Serializable {
    private String color;
    private String collaborativeEqualDuration;
    private String staffPreference;
    private String timeRequirements;
    private String type;
    private String title;
    private String duration;
    private String price;
    private String id;
    private String recurrenceEnabled;
    private String startTimeInfo;
    private String paddingRight;
    private String info;
    private String recurrenceFrequencies;
    private String visibility;
    private String staffPreferenceSettings;
    private String capacityMax;
    private String unitsMin;
    private String slotLength;
    private String packageUnassigned;
    private String paddingLeft;
    private String limitPeriod;
    private String oneBookingPerSlot;
    private String deposit;
    private String capacityMin;
    private String endTimeInfo;
    private String position;
    private String unitsMax;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCollaborativeEqualDuration() {
        return collaborativeEqualDuration;
    }

    public void setCollaborativeEqualDuration(String collaborativeEqualDuration) {
        this.collaborativeEqualDuration = collaborativeEqualDuration;
    }

    public String getStaffPreference() {
        return staffPreference;
    }

    public void setStaffPreference(String staffPreference) {
        this.staffPreference = staffPreference;
    }

    public String getTimeRequirements() {
        return timeRequirements;
    }

    public void setTimeRequirements(String timeRequirements) {
        this.timeRequirements = timeRequirements;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecurrenceEnabled() {
        return recurrenceEnabled;
    }

    public void setRecurrenceEnabled(String recurrenceEnabled) {
        this.recurrenceEnabled = recurrenceEnabled;
    }

    public String getStartTimeInfo() {
        return startTimeInfo;
    }

    public void setStartTimeInfo(String startTimeInfo) {
        this.startTimeInfo = startTimeInfo;
    }

    public String getPaddingRight() {
        return paddingRight;
    }

    public void setPaddingRight(String paddingRight) {
        this.paddingRight = paddingRight;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRecurrenceFrequencies() {
        return recurrenceFrequencies;
    }

    public void setRecurrenceFrequencies(String recurrenceFrequencies) {
        this.recurrenceFrequencies = recurrenceFrequencies;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getStaffPreferenceSettings() {
        return staffPreferenceSettings;
    }

    public void setStaffPreferenceSettings(String staffPreferenceSettings) {
        this.staffPreferenceSettings = staffPreferenceSettings;
    }

    public String getCapacityMax() {
        return capacityMax;
    }

    public void setCapacityMax(String capacityMax) {
        this.capacityMax = capacityMax;
    }

    public String getUnitsMin() {
        return unitsMin;
    }

    public void setUnitsMin(String unitsMin) {
        this.unitsMin = unitsMin;
    }

    public String getSlotLength() {
        return slotLength;
    }

    public void setSlotLength(String slotLength) {
        this.slotLength = slotLength;
    }

    public String getPackageUnassigned() {
        return packageUnassigned;
    }

    public void setPackageUnassigned(String packageUnassigned) {
        this.packageUnassigned = packageUnassigned;
    }

    public String getPaddingLeft() {
        return paddingLeft;
    }

    public void setPaddingLeft(String paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    public String getLimitPeriod() {
        return limitPeriod;
    }

    public void setLimitPeriod(String limitPeriod) {
        this.limitPeriod = limitPeriod;
    }

    public String getOneBookingPerSlot() {
        return oneBookingPerSlot;
    }

    public void setOneBookingPerSlot(String oneBookingPerSlot) {
        this.oneBookingPerSlot = oneBookingPerSlot;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getCapacityMin() {
        return capacityMin;
    }

    public void setCapacityMin(String capacityMin) {
        this.capacityMin = capacityMin;
    }

    public String getEndTimeInfo() {
        return endTimeInfo;
    }

    public void setEndTimeInfo(String endTimeInfo) {
        this.endTimeInfo = endTimeInfo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUnitsMax() {
        return unitsMax;
    }

    public void setUnitsMax(String unitsMax) {
        this.unitsMax = unitsMax;
    }

    @Override
    public String toString() {
        return
                "ResponseDTO{" +
                        "color = '" + color + '\'' +
                        ",collaborative_equal_duration = '" + collaborativeEqualDuration + '\'' +
                        ",staff_preference = '" + staffPreference + '\'' +
                        ",time_requirements = '" + timeRequirements + '\'' +
                        ",type = '" + type + '\'' +
                        ",title = '" + title + '\'' +
                        ",duration = '" + duration + '\'' +
                        ",price = '" + price + '\'' +
                        ",id = '" + id + '\'' +
                        ",recurrence_enabled = '" + recurrenceEnabled + '\'' +
                        ",start_time_info = '" + startTimeInfo + '\'' +
                        ",padding_right = '" + paddingRight + '\'' +
                        ",info = '" + info + '\'' +
                        ",recurrence_frequencies = '" + recurrenceFrequencies + '\'' +
                        ",visibility = '" + visibility + '\'' +
                        ",staff_preference_settings = '" + staffPreferenceSettings + '\'' +
                        ",capacity_max = '" + capacityMax + '\'' +
                        ",units_min = '" + unitsMin + '\'' +
                        ",slot_length = '" + slotLength + '\'' +
                        ",package_unassigned = '" + packageUnassigned + '\'' +
                        ",padding_left = '" + paddingLeft + '\'' +
                        ",limit_period = '" + limitPeriod + '\'' +
                        ",one_booking_per_slot = '" + oneBookingPerSlot + '\'' +
                        ",deposit = '" + deposit + '\'' +
                        ",capacity_min = '" + capacityMin + '\'' +
                        ",end_time_info = '" + endTimeInfo + '\'' +
                        ",position = '" + position + '\'' +
                        ",units_max = '" + unitsMax + '\'' +
                        "}";
    }
}