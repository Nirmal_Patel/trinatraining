package com.nirmal.trinatraining.model;

public class CriticalOrderHistory {
    private String startdate;
    private String status;
    private String id;
    private String order_key;
    private String initial_payment;
    private String spayment_transaction_id;
    private String video_service_id;

    public CriticalOrderHistory(String order_key, String id, String startdate, String status, String initial_payment,
                                String sPayment_transaction_id, String video_service_id) {
        this.order_key = order_key;
        this.id = id;
        this.startdate = startdate;
        this.status = status;
        this.initial_payment = initial_payment;
        this.spayment_transaction_id = sPayment_transaction_id;
        this.video_service_id = video_service_id;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInitial_payment() {
        return initial_payment;
    }

    public void setInitial_payment(String initial_payment) {
        this.initial_payment = initial_payment;
    }

    public String getOrder_key() {
        return order_key;
    }

    public void setOrder_key(String order_key) {
        this.order_key = order_key;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getVideo_service_id() {
        return video_service_id;
    }

    public void setVideo_service_id(String video_service_id) {
        this.video_service_id = video_service_id;
    }


    public String getSpayment_transaction_id() {
        return spayment_transaction_id;
    }

    public void setSpayment_transaction_id(String spayment_transaction_id) {
        this.spayment_transaction_id = spayment_transaction_id;
    }
}
