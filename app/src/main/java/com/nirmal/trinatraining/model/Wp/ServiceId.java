package com.nirmal.trinatraining.model.Wp;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceId implements Serializable {

    @SerializedName("category_id")
    private Object mCategoryId;
    @SerializedName("duration")
    private String mDuration;
    @SerializedName("id")
    private String mId;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("title")
    private String mTitle;

    public Object getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(Object categoryId) {
        mCategoryId = categoryId;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
