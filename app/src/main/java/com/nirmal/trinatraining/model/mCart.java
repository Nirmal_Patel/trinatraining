package com.nirmal.trinatraining.model;

public class mCart {

    private String count;
    private String price;
    private String date;
    private ProductItem product;
    private String ItemId;

    public mCart(String count, String price, String date, String ItemId, ProductItem product) {
        this.count = count;
        this.price = price;
        this.date = date;
        this.product = product;
        this.ItemId = ItemId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ProductItem getProduct() {
        return product;
    }

    public void setProduct(ProductItem product) {
        this.product = product;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

}
