package com.nirmal.trinatraining.model;

public class Vedio {

    private String post_title;
    private String post_status;
    private String post_type;
    private String post_content;
    private String post_date;
    private String ID;
    private String post_author;
    private String video_url;
    private String image_url;

    public Vedio(String post_title, String post_status, String post_type, String post_content,
                 String post_date, String ID, String post_author, String video_url, String image_url) {
        this.post_title = post_title;
        this.post_status = post_status;
        this.post_type = post_type;
        this.post_content = post_content;
        this.post_date = post_date;
        this.ID = ID;
        this.post_author = post_author;
        this.video_url = video_url;
        this.image_url = image_url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPost_author() {
        return post_author;
    }

    public void setPost_author(String post_author) {
        this.post_author = post_author;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }


}
