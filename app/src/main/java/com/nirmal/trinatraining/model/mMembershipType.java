package com.nirmal.trinatraining.model;

public class mMembershipType {


    private String sId;
    private String sName;
    private String sInitial_payment;
    private String sExpiration_number;
    private String sexpiration_period;

    public mMembershipType(String sId, String sName, String sInitial_payment,
                           String sExpiration_number, String sexpiration_period) {
        this.sId = sId;
        this.sName = sName;
        this.sInitial_payment = sInitial_payment;
        this.sExpiration_number = sExpiration_number;
        this.sexpiration_period = sexpiration_period;
    }

    public String getsExpiration_number() {
        return sExpiration_number;
    }

    public void setsExpiration_number(String sExpiration_number) {
        this.sExpiration_number = sExpiration_number;
    }

    public String getSexpiration_period() {
        return sexpiration_period;
    }

    public void setSexpiration_period(String sexpiration_period) {
        this.sexpiration_period = sexpiration_period;
    }


    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsInitial_payment() {
        return sInitial_payment;
    }

    public void setsInitial_payment(String sInitial_payment) {
        this.sInitial_payment = sInitial_payment;
    }
}
