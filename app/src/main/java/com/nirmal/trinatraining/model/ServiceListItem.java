package com.nirmal.trinatraining.model;

public class ServiceListItem {

    String id, price, title, duration;
    boolean isSelected;

    public ServiceListItem(String id, String price, String title, String duration, boolean isSelected) {
        this.id = id;
        this.price = price;
        this.isSelected = isSelected;
        this.title = title;
        this.duration = duration;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
