package com.nirmal.trinatraining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostResponse {
    @SerializedName("id")
    @Expose
    private String auth;

    public String getAuth() {
        return auth;
    }

    public void setId(String auth) {
        this.auth = auth;
    }
}
