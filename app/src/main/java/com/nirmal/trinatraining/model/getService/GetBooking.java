package com.nirmal.trinatraining.model.getService;

import java.io.Serializable;

public class GetBooking implements Serializable {


    private String id;
    private String category_id;
    private String title;
    private String duration;
    private String price;
    private String customer_id;
    private String number_of_persons;
    private String units;
    private String status;
    private String notes;
    private String start_date;
    private String end_date;
    private String appoinment_id;

    public GetBooking(String id, String category_id, String title, String duration,
                      String price, String customer_id, String number_of_persons,
                      String units, String status, String notes, String start_date,
                      String end_date, String appoinment_id) {
        this.id = id;
        this.category_id = category_id;
        this.title = title;
        this.duration = duration;
        this.price = price;
        this.customer_id = customer_id;
        this.number_of_persons = number_of_persons;
        this.units = units;
        this.status = status;
        this.notes = notes;
        this.start_date = start_date;
        this.end_date = end_date;
        this.appoinment_id = appoinment_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getNumber_of_persons() {
        return number_of_persons;
    }

    public void setNumber_of_persons(String number_of_persons) {
        this.number_of_persons = number_of_persons;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAppoinment_id() {
        return appoinment_id;
    }

    public void setAppoinment_id(String appoinment_id) {
        this.appoinment_id = appoinment_id;
    }


}
