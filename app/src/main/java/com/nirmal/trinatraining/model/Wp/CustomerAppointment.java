package com.nirmal.trinatraining.model.Wp;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerAppointment implements Serializable {

    @SerializedName("customer_id")
    private String mCustomerId;
    @SerializedName("notes")
    private Object mNotes;
    @SerializedName("number_of_persons")
    private String mNumberOfPersons;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("units")
    private String mUnits;

    public String getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(String customerId) {
        mCustomerId = customerId;
    }

    public Object getNotes() {
        return mNotes;
    }

    public void setNotes(Object notes) {
        mNotes = notes;
    }

    public String getNumberOfPersons() {
        return mNumberOfPersons;
    }

    public void setNumberOfPersons(String numberOfPersons) {
        mNumberOfPersons = numberOfPersons;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUnits() {
        return mUnits;
    }

    public void setUnits(String units) {
        mUnits = units;
    }

}
