package com.nirmal.trinatraining.model;

public class VideoItem {

    private String id;
    private String post_title;
    private String post_content;
    private String post_date;
    private String image_url;
    private String reguler_price;
    private String sale_price;

    public VideoItem(String id, String post_title, String post_content,
                     String post_date, String image_url, String reguler_price, String sale_price) {
        this.id = id;
        this.post_title = post_title;
        this.post_content = post_content;
        this.post_date = post_date;
        this.image_url = image_url;
        this.reguler_price = reguler_price;
        this.sale_price = sale_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getReguler_price() {
        return reguler_price;
    }

    public void setReguler_price(String reguler_price) {
        this.reguler_price = reguler_price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }
}
