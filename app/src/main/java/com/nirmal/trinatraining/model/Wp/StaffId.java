package com.nirmal.trinatraining.model.Wp;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StaffId implements Serializable {

    @SerializedName("attachment_id")
    private String mAttachmentId;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("full_name")
    private String mFullName;
    @SerializedName("id")
    private String mId;
    @SerializedName("info")
    private String mInfo;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("wp_user_id")
    private String mWpUserId;

    public String getAttachmentId() {
        return mAttachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        mAttachmentId = attachmentId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getWpUserId() {
        return mWpUserId;
    }

    public void setWpUserId(String wpUserId) {
        mWpUserId = wpUserId;
    }

}
