package com.nirmal.trinatraining.model.Wp;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SList implements Serializable {

    @SerializedName("created")
    private String mCreated;
    @SerializedName("created_from")
    private String mCreatedFrom;
    @SerializedName("custom_service_name")
    private Object mCustomServiceName;
    @SerializedName("custom_service_price")
    private Object mCustomServicePrice;
    @SerializedName("customer_appointment")
    private CustomerAppointment mCustomerAppointment;
    @SerializedName("end_date")
    private String mEndDate;
    @SerializedName("extras_duration")
    private String mExtrasDuration;
    @SerializedName("google_event_etag")
    private String mGoogleEventEtag;
    @SerializedName("google_event_id")
    private String mGoogleEventId;
    @SerializedName("id")
    private String mId;
    @SerializedName("internal_note")
    private String mInternalNote;
    @SerializedName("location_id")
    private Object mLocationId;
    @SerializedName("outlook_event_change_key")
    private Object mOutlookEventChangeKey;
    @SerializedName("outlook_event_id")
    private Object mOutlookEventId;
    @SerializedName("outlook_event_series_id")
    private Object mOutlookEventSeriesId;
    @SerializedName("service_id")
    private ServiceId mServiceId;
    @SerializedName("staff_any")
    private String mStaffAny;
    @SerializedName("staff_id")
    private StaffId mStaffId;
    @SerializedName("start_date")
    private String mStartDate;

    public String getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
        mCreated = created;
    }

    public String getCreatedFrom() {
        return mCreatedFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        mCreatedFrom = createdFrom;
    }

    public Object getCustomServiceName() {
        return mCustomServiceName;
    }

    public void setCustomServiceName(Object customServiceName) {
        mCustomServiceName = customServiceName;
    }

    public Object getCustomServicePrice() {
        return mCustomServicePrice;
    }

    public void setCustomServicePrice(Object customServicePrice) {
        mCustomServicePrice = customServicePrice;
    }

    public CustomerAppointment getCustomerAppointment() {
        return mCustomerAppointment;
    }

    public void setCustomerAppointment(CustomerAppointment customerAppointment) {
        mCustomerAppointment = customerAppointment;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        mEndDate = endDate;
    }

    public String getExtrasDuration() {
        return mExtrasDuration;
    }

    public void setExtrasDuration(String extrasDuration) {
        mExtrasDuration = extrasDuration;
    }

    public String getGoogleEventEtag() {
        return mGoogleEventEtag;
    }

    public void setGoogleEventEtag(String googleEventEtag) {
        mGoogleEventEtag = googleEventEtag;
    }

    public String getGoogleEventId() {
        return mGoogleEventId;
    }

    public void setGoogleEventId(String googleEventId) {
        mGoogleEventId = googleEventId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInternalNote() {
        return mInternalNote;
    }

    public void setInternalNote(String internalNote) {
        mInternalNote = internalNote;
    }

    public Object getLocationId() {
        return mLocationId;
    }

    public void setLocationId(Object locationId) {
        mLocationId = locationId;
    }

    public Object getOutlookEventChangeKey() {
        return mOutlookEventChangeKey;
    }

    public void setOutlookEventChangeKey(Object outlookEventChangeKey) {
        mOutlookEventChangeKey = outlookEventChangeKey;
    }

    public Object getOutlookEventId() {
        return mOutlookEventId;
    }

    public void setOutlookEventId(Object outlookEventId) {
        mOutlookEventId = outlookEventId;
    }

    public Object getOutlookEventSeriesId() {
        return mOutlookEventSeriesId;
    }

    public void setOutlookEventSeriesId(Object outlookEventSeriesId) {
        mOutlookEventSeriesId = outlookEventSeriesId;
    }

    public ServiceId getServiceId() {
        return mServiceId;
    }

    public void setServiceId(ServiceId serviceId) {
        mServiceId = serviceId;
    }

    public String getStaffAny() {
        return mStaffAny;
    }

    public void setStaffAny(String staffAny) {
        mStaffAny = staffAny;
    }

    public StaffId getStaffId() {
        return mStaffId;
    }

    public void setStaffId(StaffId staffId) {
        mStaffId = staffId;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        mStartDate = startDate;
    }

}
